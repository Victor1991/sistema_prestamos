<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Recuperar contraseña | Administración CMS</title>

        <!--Core CSS -->
        <link href="<?php echo admin_assets('bs3/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo admin_assets('css/bootstrap-reset.css'); ?>" rel="stylesheet">
        <link href="<?php echo admin_assets('font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />

        <!-- Custom styles for this template -->
        <link href="<?php echo admin_assets('css/style.css'); ?>" rel="stylesheet">
        <link href="<?php echo admin_assets('css/style-responsive.css'); ?>" rel="stylesheet" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="login-body">

        <div class="container">
            <form method="post" class="form-signin" action="<?php echo base_url('login/recuperar_password'); ?>">
                <div class="login-header">
                    <img src="<?php echo admin_assets('images/logo-login.png'); ?>">
                </div>
                <div class="login-wrap">
                    <div class="alert alert-info">
                        <p><i class="fa fa-info-circle"></i> Por favor, escriba su correo electrónico. Recibirá un enlace por correo electrónico para crear una nueva contraseña.</p>
                    </div>
                    <div class="user-login-info">
                        <input type="email" name="correo" id="correo" class="form-control" placeholder="Correo electrónico">
                    </div>
                    <button type="button" class="btn btn-primary btn-block btn-lg btn-recover">Continuar <i class="fa fa-arrow-right"></i></button>                    
                </div>
            </form>
            <div class="login-exta-links">
                <a href="<?php echo base_url('login'); ?>">Accesar</a>
            </div>
        </div>


        <!-- Javascript Libraries -->
        <script src="<?php echo admin_assets('js/jquery.js'); ?>"></script>
        <script src="<?php echo admin_assets('bs3/js/bootstrap.min.js'); ?>"></script>
        <script type="text/javascript">
            $('.btn-recover').click(function(e) {
                e.preventDefault();
                $.ajax({
                    url : '/login/recuperar_password',
                    data : { correo : $('#correo').val() },
                    type : 'POST',
                    dataType : 'json',
                    beforeSend: function () {
                        $('.login-wrap').html('<div align="center">Procesando solicitud...<img src="<?php echo admin_assets('img/circular_load.GIF'); ?>" style="width: 32px;"></div>');
                    },
                    success : function(json) {
                        if(json.estatus == 'ok') {
                            $('.login-wrap').html('<div class="alert alert-success"><p>Se han enviado a su correo las intrucciones para restablecer su contraseña</p></div>');
                        } else {
                            $('.login-wrap').html('<div class="alert alert-danger"><p>'+json.mensaje+'</p></div>');
                        }
                    },
                    error : function(xhr, status) {
                        $('.login-wrap').html('<div class="alert alert-danger"><p>Por el momento no es posible realizar el proceso para restablecer su contraseña (Error '+status+')</p></div>');
                    }
                });
            });
        </script>

    </body>
</html>