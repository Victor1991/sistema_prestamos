<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Cambiar contraseña | Administración CMS</title>

        <!--Core CSS -->
        <link href="<?php echo admin_assets('bs3/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo admin_assets('css/bootstrap-reset.css'); ?>" rel="stylesheet">
        <link href="<?php echo admin_assets('font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />

        <!-- Custom styles for this template -->
        <link href="<?php echo admin_assets('css/style.css'); ?>" rel="stylesheet">
        <link href="<?php echo admin_assets('css/style-responsive.css'); ?>" rel="stylesheet" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="login-body">

        <div class="container">
            <form method="post" class="form-signin" action="">
                <div class="login-header">
                    <img src="<?php echo admin_assets('images/logo-login.png'); ?>">
                </div>                
                <div class="login-wrap">
                    <?php echo show_alerts(); ?>
                    <?php if (!$mensaje_cambio): ?>
                    <p>Ingrese su nueva contraseña</p>
                    <div class="user-login-info">
                        <input type="password" name="password" id="password" class="form-control" placeholder="Nueva contraseña">
                        <input type="password" name="conf_password" id="conf_password" class="form-control" placeholder="Vuelva a escribir su contraseña">
                    </div>
                    <button type="submit" class="btn btn-primary btn-block btn-lg">Continuar <i class="fa fa-arrow-right"></i></button>
                    <?php endif; ?>
                </div>
            </form>                        
            <div class="login-exta-links">
                <a href="<?php echo base_url('login'); ?>">Accesar</a>
            </div>
        </div>


        <!-- Javascript Libraries -->
        <script src="<?php echo admin_assets('js/jquery.js'); ?>"></script>
        <script src="<?php echo admin_assets('bs3/js/bootstrap.min.js'); ?>"></script>

    </body>
</html>