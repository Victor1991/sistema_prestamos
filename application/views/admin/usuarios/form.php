<div class="row">
     <div class="col-lg-12">
          <!-- Default Card Example -->
          <div class="card shadow mb-4">
               <!-- Card Header - Dropdown -->
               <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary"><?=$titulo_form?></h6>
               </div>
               <!-- Card Body -->
               <div class="card-body">
                    <?php if ($errors): ?>

                         <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                              </button>
                              <p><?= $errors ?></p>
                         </div>

                    <?php endif; ?>


                    <form method="post" action="" id="myCoolForm">


                         <div class="row">
                              <div class="form-group col">
                                   <label>Nombres</label>
                                   <input type="text" name="nombres" class="form-control" value="<?=$nombres;?>">
                              </div>

                              <div class="form-group col">
                                   <label>Apellido paterno</label>
                                   <input type="text" name="apellido_paterno" class="form-control" value="<?=$apellido_paterno;?>">

                              </div>

                              <div class="form-group col">
                                   <label>Apellido materno</label>
                                   <input type="text" name="apellido_materno" class="form-control" value="<?=$apellido_materno;?>">

                              </div>

                         </div>

                         <div class="row">
                              <div class="form-group col-5">
                                   <label>Usuario</label>
                                   <input type="text" name="nombre" class="form-control" value="<?=$username;?>">
                              </div>
                              <div class="form-group col-5">
                                   <label>Rol</label>
                                   <?=  form_dropdown('rol', $roles, $role_id , 'class="form-control"')?>
                              </div>


                              <div class="form-group col-2">
                                   <label>Estatus</label>
                                   <div class="onoffswitch">
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="estatus" class="onoffswitch-checkbox" value="1" id="myonoffswitch" <?php if($active==1) echo "checked='checked' "; ?>>
                                            <label class="onoffswitch-label" for="myonoffswitch"></label>
                                        </div>
                                   </div>
                              </div>

                         </div>






                         <div class="form-group">
                              <label>Correo</label>
                              <input type="email" name="email" class="form-control" value="<?=$email;?>">
                         </div>

                         <div class="row">
                              <div class="form-group col">
                                   <label><?=$txt_contrasena?></label>
                                   <input type="password" name="password" class="form-control" id="password">
                              </div>


                              <div class="form-group col">
                                   <label><?=$txt_confirmar?></label>
                                   <input type="password" name="confirmar_password" class="form-control" id="confirmar_password">
                              </div>
                         </div>

                         <div class="float-right">
                              <button type="submit" class="btn btn-success">Guardar</button>
                              <a href="<?=base_url('admin/usuarios')?>" class="btn bg-gray-500 text-gray-100">Cancelar</a>
                         </div>

                    </form>
               </div>
          </div>
     </div>
</div>
