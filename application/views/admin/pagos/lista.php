<div class="row">
     <div class="col-lg-12">
          <!-- Default Card Example -->
          <div class="card shadow mb-4">
               <!-- Card Header - Dropdown -->
               <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Listado de Prestamos</h6>
                    <a href="<?= base_url('admin/prestamos/form') ?>" class="btn btn-success" style="color: #FFFFFF;">

                         <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Prestamo
                    </a>
               </div>
               <!-- Card Body -->
               <div class="card-body">
                    <?php if ($errors): ?>
                         <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                              </button>
                              <p><?= $errors ?></p>
                         </div>
                    <?php endif; ?>

                    <?php if ($messages): ?>
                         <div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                              </button>
                              <p><?= $messages ?></p>
                         </div>
                    <?php endif; ?>

                    <div class="table-responsive" style="border: 0px !important;">
                         <div>
                              <form method="get" action="<?php echo base_url('admin/usuarios'); ?>" class="row" style="width:100%">
                                   <div class="col-sm-8">
                                        <div class="form-group">
                                             <label>Buscar:</label>
                                             <input type="text" name="username" id="buscar" class="form-control" onkeypress="filtro()">
                                        </div>
                                   </div>


                                   <div class="col-sm-4">
                                        <div class="form-group m-t-25">
                                             <label> &nbsp;</label>
                                             <a id="f_btn_cancelar" class="btn btn-warning btn-block cancel" onclick="Cancelar()">Cancelar</a>
                                        </div>
                                   </div>
                              </form>

                         </div>

                         <hr>

                         <!--  -->
                         <div id="filter-table-data">
                              <table id="tbl-posts" class="table table-striped table-bordered table-hover dataTables-users" >
                                   <thead>
                                        <tr>
                                             <th data-column-id="usu.nombre_completo">#</th>
                                             <th data-column-id="usu.role_id">Cliente</th>
                                             <th data-column-id="usu.email">Prestamo</th>
                                             <th data-column-id="usu.active">Meses</th>
                                             <th data-column-id="usu.active">Fecha Inicio</th>
                                             <th class="text-center"><i class="fa fa-cogs"></i></th>
                                        </tr>
                                   </thead>
                                   <tbody id="tcontent">
                                   </tbody>
                              </table>
                              <div id="page">
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>



</div>
