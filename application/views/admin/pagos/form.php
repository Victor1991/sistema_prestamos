<div class="row">
     <div class="col-lg-12">
          <!-- Default Card Example -->
          <div class="card shadow mb-4">
               <!-- Card Header - Dropdown -->
               <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary"><?=$titulo_form?></h6>
               </div>
               <!-- Card Body -->
               <div class="card-body">
                    <?php if ($errors): ?>

                         <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                              </button>
                              <p><?= $errors ?></p>
                         </div>

                    <?php endif; ?>

                    <?php $this->load->view('admin/pagos/form_pago') ?>

                    <div class="float-right">
                         <a href="<?=base_url('admin/usuarios')?>" class="btn bg-gray-500 text-gray-100">Cancelar</a>
                    </div>

               </div>
          </div>
     </div>
</div>


<div id="modalimagen" class="modal">
     <!-- The Close Button -->
     <span class="close" onclick="$('#modalimagen').modal('hide');">&times;</span>

     <!-- Modal Content (The Image) -->
     <img class="modal-content" id="img01">

     <!-- Modal Caption (Image Text) -->
     <div id="caption"></div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_pago" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
          <form action="<?=base_url('admin/pagos/guardar_pago')?>" id="form_pago" method="post" enctype="multipart/form-data">
               <div class="modal-content">
                    <div class="modal-body text-center">
                         <h3>Cantidad a pagar</h3>
                         <input type="hidden" name="prestamo_id" value="<?=$id?>">
                         <div class="input-group">
                              <input type="text" required class="form-control bg-light  border-0 small" placeholder="Cantidad" name="monto" onkeypress="return filterFloat(event,this);">
                         </div>
                         <br>
                         <div class="input-group">
                              <select class="form-control" name="tipo_pago_id" required>
                                   <option> -- Selecciona tipo pago --</option>
                                   <?php foreach ($cat_pagos as $key => $pago): ?>
                                        <option value="<?=$pago->id?>"><?=$pago->nombre?></option>
                                   <?php endforeach; ?>
                              </select>
                         </div>
                         <br>
                         <div class="input-group">
                              <input class="form-control datepickern nuevo"  required value="" id="fecha_pago" name="fecha_pago" type="text">
                         </div>
                         <br>
                         <div class="form-group col-md-12">
                              <input type="file" accept="image/*" onchange="loadFile(event)" name="foto1" id="foto"><br><br>
                              <img id="foto1" src="<?=base_url('assets/img/no-image.jpg')?>" class="img-thumbnail"/>

                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="submit" class="btn btn-success">Generar</button>
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
               </div>
          </form>
     </div>
</div>

<style>
.m-b-m{
     margin-bottom: 10px;
}
</style>
