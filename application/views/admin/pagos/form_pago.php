<form method="post" action="<?=base_url('admin/prestamos/prestamos_data')?>" id="form_prestamos" enctype="multipart/form-data">

     <input type="hidden" name="tabla" id="tabla">
     <input type="hidden" name="intereses" id="intereses">
     <input type="hidden" name="id" id="prestamo_id" id="prestamo_id" value="<?=$id?>">
     <input type="hidden" id="creado" value="<?=$creado?>">

     <div class="row">

          <?php if(isset($prestamo['prestamo_padre'])):?>
              <div class="form-group col-md-4">
                   <label>Monto adeudado prestamo anteriror</label>
                   <div class="form-group">
                        <div class="input-group mb-4">
                             <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                             </div>
                             <input <?php if($prestamo['id']):?> readonly <?php endif; ?> class="form-control" required type="text" value="<?=$prestamo['cantidad_anterior']?>" id="monto"  onkeypress="return filterFloat(event,this);">
                        </div>
                   </div>
              </div>

              <div class="form-group col-md-4">
                  <label>Monto nuevo prestamo</label>
                  <div class="form-group">
                       <div class="input-group mb-4">
                             <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                             </div>
                             <input <?php if($prestamo['id']):?> readonly <?php endif; ?> class="form-control" required type="text" value="<?=$prestamo['monto']?>" id="monto"  onkeypress="return filterFloat(event,this);">
                       </div>
                  </div>
              </div>

              <div class="form-group col-md-4">
                  <label>Monto adeudado</label>
                  <div class="form-group">
                       <div class="input-group mb-4">
                             <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                             </div>
                             <input <?php if($prestamo['id']):?> readonly <?php endif; ?> class="form-control" required type="text" value="<?=$prestamo['cantidad_anterior'] + $prestamo['monto'] ?>" id="monto"  onkeypress="return filterFloat(event,this);">
                       </div>
                  </div>
              </div>
         <?php else: ?>
              <div class="form-group col-md-4">
                  <label>Monto adeudado</label>
                  <div class="form-group">
                       <div class="input-group mb-4">
                             <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                             </div>
                             <input <?php if($id):?> readonly <?php endif; ?> class="form-control" required type="text" value="<?=$monto?>" id="monto"  onkeypress="return filterFloat(event,this);">
                       </div>
                  </div>
              </div>

         <?php endif; ?>

          <div class="form-group col-md-4">
               <label>Enganche</label>
               <div class="form-group">
                    <div class="input-group mb-4">
                         <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                         </div>
                         <input <?php if($id):?> readonly <?php endif; ?> class="form-control" type="text" value="<?=$enganche?>" id="enganche" name="enganche" onkeypress="return filterFloat(event,this);">
                    </div>
               </div>
          </div>

          <div class="form-group col-md-4">
               <label>Tasa interes</label>
               <div class="form-group">
                    <div class="input-group mb-4">
                         <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-percentage"></i></span>
                         </div>
                         <input <?php if($id):?> readonly <?php endif; ?> class="form-control solo-numero" required  value="<?=$interes?>" id="interes" name="interes" type="text" name="pin" maxlength="2" >
                    </div>
               </div>
          </div>

          <div class="form-group col-md-4">
               <label>Plazo en meses</label>
               <div class="form-group">
                    <div class="input-group mb-4">
                         <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                         </div>
                         <input <?php if($id):?> readonly <?php endif; ?> class="form-control solo-numero" required value="<?=$meses?>" id="meses" name="meses" type="text" name="pin" maxlength="2" >
                    </div>
               </div>
          </div>

          <div class="form-group col-md-4">
               <label>Fecha Inicio</label>
               <div class="form-group">
                    <div class="input-group mb-4">
                         <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                         </div>
                         <input <?php if($id):?> readonly <?php endif; ?> class="form-control  <?php if(!$id):?> datepickern<?php endif; ?> " required value="<?=$fecha_inicio?>" id="fecha_inicio" name="fecha_inicio" type="text"  >
                    </div>
               </div>
          </div>

          <div class="form-group col-md-4">
               <label>Cuota mensual</label>
               <div class="form-group" style="margin-bottom: -20px;">
                    <div class="input-group mb-4">
                         <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                         </div>
                         <input <?php if($id):?> readonly <?php endif; ?> class="form-control solo-numero" required value="<?=$cuota_mensual?>" id="cuota_mensual" name="cuota_mensual" type="text" >
                    </div>
               </div>
               <span id="recomendada"> Cuota mensual recomendada  </span>
          </div>

     </div>

     <div class="row" style="display:none;">
          <div class="col-md-4 m-b-m">
               <button type="button" class="btn btn-secondary btn-block">
                    Tasa interes <br><span class="badge badge-light" id="tasa_interes">  0</span>
               </button>
          </div>
          <div class="col-md-4 m-b-m">
               <button type="button" class="btn btn-secondary btn-block">
                    Interes general <br><span class="badge badge-light" id="interes_general"> 0</span>
               </button>
          </div>
          <div class="col-md-4 m-b-m">
               <button type="button" class="btn btn-secondary btn-block">
                    Tasa interes anual<br><span class="badge badge-light" id="n"> 0</span>
               </button>
          </div>
          <div class="col-md-4 m-b-m">
               <button type="button" class="btn btn-secondary btn-block">
                    Cuota mensual <br><span class="badge badge-light" id="cuota_mensual"> 0</span>
               </button>
          </div>
     </div>
     <br>
     <div class="row">
          <div class="col-md-12">
               <a onclick="pagar()" class="btn btn-success btn-icon-split float-right">
                    <span class="icon text-white-50">
                         <i class="fas fa-money-bill-wave"></i>
                    </span>
                    <span class="text">Pagar</span>
               </a>
          </div>
     </div>

     <br>
     <div class="row">
          <div class="col-md-12">
               <div class="table-responsive">

                    <table class="table table-hover table-bordered">
                         <thead>
                              <tr>
                                   <th scope="col"># PAGOS</th>
                                   <th scope="col">CAPITAL AMORTIZADO</th>
                                   <th scope="col">INTERES</th>
                                   <th scope="col">CUOTA</th>
                                   <th scope="col">AMORTIZACION</th>
                                   <th scope="col">MES / AÑO</th>
                                   <th scope="col">PAGOS</th>
                              </tr>
                         </thead>
                         <tbody  id="myTable">
                              <tr>
                                   <th colspan="6" class="text-center">SI DATOS A COTIZAR</th>
                              </tr>
                         </tbody>
                    </table>
               </div>
          </div>
     </div>
     
</form>
