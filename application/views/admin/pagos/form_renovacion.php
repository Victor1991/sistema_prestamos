<div class="row">
     <div class="col-lg-12">
          <!-- Default Card Example -->
          <div class="card shadow mb-4">
               <!-- Card Header - Dropdown -->
               <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary"><?=$titulo_form?></h6>
               </div>
               <!-- Card Body -->
               <div class="card-body">
                    <?php if ($errors): ?>

                         <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                              </button>
                              <p><?= $errors ?></p>
                         </div>

                    <?php endif; ?>


                         <div class="row">
                              <div class="col-md-12">
                                   <div class="card shadow mb-12">
                                        <!-- Card Header - Accordion -->
                                        <a href="#collapseCardExample" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
                                             <h6 class="m-0 font-weight-bold text-primary">Prestamo principal</h6>
                                        </a>
                                        <!-- Card Content - Collapse -->
                                        <div class="collapse" id="collapseCardExample" style="">
                                             <div class="card-body">
                                                  <div class="col-md-12">
                                                       <?php $this->load->view('admin/renovacion/info_prestamo') ?>

                                                       <hr>
                                                       <?php $this->load->view('admin/renovacion/tabla_prestamos') ?>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <?php if(count($prestamos_hijo) > 0):?>
                              <?php foreach ($prestamos_hijo as $key => $prestamo_hijo): ?>
                                   <br>
                                   <div class="row">
                                        <div class="col-md-12">
                                             <div class="card shadow mb-12">
                                                  <!-- Card Header - Accordion -->
                                                  <a href="#collapseCardExample<?=$key?>" class="d-block card-header py-3
                                                  <?php if($prestamo_hijo != end( $prestamos_hijo )): ?>
                                                       collapsed
                                                  <?php endif; ?>
                                                  " data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
                                                       <h6 class="m-0 font-weight-bold text-primary">Renovacion prestamos <?=$key+1?> </h6>
                                                  </a>
                                                  <!-- Card Content - Collapse -->
                                                  <div class="collapse
                                                  <?php if($prestamo_hijo == end( $prestamos_hijo )): ?>
                                                       show
                                                  <?php endif; ?>
                                                       " id="collapseCardExample<?=$key?>" style="">
                                                       <div class="card-body">
                                                            <div class="col-md-12">
<?php if($prestamo_hijo != end( $prestamos_hijo )): ?>
                                                                 <?php $this->load->view('admin/renovacion/info_prestamo', array('prestamo' => $prestamo_hijo['prestamo'])) ?>
                                                                 <hr>
<?php endif; ?>
                                                                 <?php if($prestamo_hijo == end( $prestamos_hijo )): ?>

                                                                      <div class="row">
                                                                           <div class="offset-md-8 col-md-4 pull-right">
                                                                                <a onclick="pagar()" class="btn btn-success btn-icon-split float-right">
                                                                                     <span class="icon text-white-50">
                                                                                          <i class="fas fa-money-bill-wave"></i>
                                                                                     </span>
                                                                                     <span class="text">Pagar</span>
                                                                                </a>
                                                                           </div>
                                                                      </div>
                                                                      <br>

                                                                 <?php endif; ?>
                                                                 <?php if($prestamo_hijo == end( $prestamos_hijo )): ?>
                                                                      <?php $this->load->view('admin/pagos/form_pago',  $prestamo_hijo['prestamo']) ?>
                                                                 <?php else: ?>
                                                                      <?php $this->load->view('admin/renovacion/tabla_prestamos', array('tabla_pagos' => $prestamo_hijo['pagos'])) ?>
                                                                 <?php endif; ?>
                         <div class="clearfix">

                         </div>

                                                            </div>
                                                       </div>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              <?php endforeach; ?>
                         <?php endif; ?>
                         <br>
                         <div class="float-right">
                              <a href="<?=base_url('admin/prestamos')?>" class="btn bg-gray-500 text-gray-100">Cancelar</a>
                         </div>


               </div>
          </div>
     </div>
</div>


<div id="modalimagen" class="modal">
     <!-- The Close Button -->
     <span class="close" onclick="$('#modalimagen').modal('hide');">&times;</span>

     <!-- Modal Content (The Image) -->
     <img class="modal-content" id="img01">

     <!-- Modal Caption (Image Text) -->
     <div id="caption"></div>
</div>

<!-- Modal -->
<?php $this->load->view('admin/pagos/modal_pago') ?>

<style>
.m-b-m{
     margin-bottom: 10px;
}
</style>
