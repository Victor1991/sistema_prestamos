<div class="modal fade" id="modal_pago" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
          <form action="<?=base_url('admin/pagos/guardar_pago')?>" id="form_pago" method="post" enctype="multipart/form-data">
               <div class="modal-content">
                    <div class="modal-body text-center">
                         <h3>Cantidad a pagar</h3>
                         <input type="hidden" name="prestamo_id" value="<?=$id?>">
                         <div class="input-group">
                              <input type="text" required class="form-control bg-light  border-0 small" placeholder="Cantidad" name="monto" onkeypress="return filterFloat(event,this);">
                         </div>
                         <br>
                         <div class="input-group">
                              <select class="form-control" name="tipo_pago_id" required>
                                   <option> -- Selecciona tipo pago --</option>
                                   <?php foreach ($cat_pagos as $key => $pago): ?>
                                        <option value="<?=$pago->id?>"><?=$pago->nombre?></option>
                                   <?php endforeach; ?>
                              </select>
                         </div>
                         <br>
                         <div class="input-group">
                              <input class="form-control datepickern nuevo"  required value="" id="fecha_pago" name="fecha_pago" type="text">
                         </div>
                         <br>
                         <div class="form-group col-md-12">
                              <input type="file" accept="image/*" onchange="loadFile(event)" name="foto1" id="foto"><br><br>
                              <img id="foto1" src="<?=base_url('assets/img/no-image.jpg')?>" class="img-thumbnail"/>

                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="submit" class="btn btn-success">Generar</button>
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
               </div>
          </form>
     </div>
</div>
