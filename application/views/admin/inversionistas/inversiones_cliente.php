<div class="row">
     <div class="col-lg-12">
          <!-- Default Card Example -->
          <div class="card shadow mb-4">
               <input type="hidden" id="cliente_id" value="<?=$cliente->id?>">
               <div class="card-body">
                    <div class="row">
                         <div class="col-md-10">
                              <h4>
                                   <strong>Cliente :</strong>
                                   <?=$cliente->nombres.' '.$cliente->apellido_paterno.' '.$cliente->apellido_materno?>
                              </h4>
                         </div>
                         <div class="col-md-2">
                              <button type="button"
                              class="btn btn-info btn-block" name="button" data-toggle="modal" data-target="#modal_aportaciones">Nueva aportación</button>
                         </div>
                    </div>
               </div>
          </div>
     </div>
     <div class="col-md-12">
          <div class="row" id="lista_proyectos">


          </div>
     </div>
     <div class="clearfix">

     </div>

</div>

<div class="modal fade" id="modal_aportaciones" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
     <div class="modal-dialog modal-lg">
          <div class="modal-content">
               <div class="modal-header">
                      <h4 class="modal-title">Aportaciones</h4>
               </div>
               <form id="form_aportacion" enctype="multipart/form-data" method="post" action="<?=base_url('admin/inversionistas/guardar_aportacion')?>">
                    <div class="modal-body">

                         <input type="hidden" name="cliente_id" value="<?=$cliente->id?>">
                         <div class="form-group">
                              <label>Proyecto:</label>
                              <select class="form-control" name="proyecto_id" required>
                                   <option value="">-- Selecciona un proyecto --</option>
                                   <?php foreach ($proyectos as $key => $proyecto): ?>
                                        <option value="<?=$proyecto->id?>"><?=$proyecto->nombre?></option>
                                   <?php endforeach; ?>
                              </select>
                         </div>
                         <div class="form-group">
                              <label>Tipo de pago:</label>
                              <select class="form-control" name="tipo_pago" required>
                                   <option value="">-- Selecciona un tipo --</option>
                                   <?php foreach ($formas_pago as $key => $forma_pago): ?>
                                        <option value="<?=$forma_pago->id?>"><?=$forma_pago->nombre?></option>
                                   <?php endforeach; ?>
                              </select>
                         </div>
                         <div class="form-group">
                              <label>Cantidad:</label>
                              <input type="text" name="cantidad" value="" required class="form-control">
                         </div>
                         <div class="form-group">
                              <label>Tipo de aportación:</label>
                              <select class="form-control" name="tipo_aportacion" required>
                                   <option value="">-- Selecciona un tipo --</option>
                                   <?php foreach ($formas_aportaciones as $key => $forma): ?>
                                        <option value="<?=$forma->id?>"><?=$forma->nombre?></option>
                                   <?php endforeach; ?>
                              </select>
                         </div>
                         <div class="form-group">
                              <label>Interes:</label>
                              <input type="text" name="interes" value="" class="form-control" required>
                         </div>
                         <div class="form-group">
                              <label>Fecha:</label>
                              <input type="text" class="form-control" name="fecha" value="">
                         </div>



                    </div>
                    <div class="modal-footer" style="padding: 5px 20px 5px 4px;">
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Guardar</button>
                         <button type="submit" class="btn btn-primary">
                              Guardar
                         </button>
                    </div>
               </form>
          </div>
     </div>
</div>
