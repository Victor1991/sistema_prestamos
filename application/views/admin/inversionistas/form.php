<div class="row">
     <div class="col-lg-12">
          <!-- Default Card Example -->
          <div class="card shadow mb-4">
               <!-- Card Header - Dropdown -->
               <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary"><?=$titulo_form?></h6>
               </div>
               <!-- Card Body -->
               <div class="card-body">
                    <?php if ($errors): ?>

                         <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                              </button>
                              <p><?= $errors ?></p>
                         </div>

                    <?php endif; ?>


                    <form method="post" action="" id="myCoolForm" enctype="multipart/form-data">
                         <div class="row">
                              <div class="form-group col-md-5">
                                   <label>Nombres</label>
                                   <input required type="text" name="nombre" class="form-control" value="<?=$nombre;?>">
                              </div>

                              <div class="form-group col-md-5">
                                   <label>Tasa interes</label>
                                   <div class="form-group">
                                        <div class="input-group mb-4">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text"><i class="fas fa-percentage"></i></span>
                                             </div>
                                             <input class="form-control solo-numero" required  value="<?=$interes;?>" id="interes" name="interes" type="text" name="pin" maxlength="5" >
                                        </div>
                                   </div>
                              </div>


                              <div class="form-group col-md-2">
                                   <label>Estatus</label><br>
                                   <input type="checkbox" value="1" name="estatus" <?php if ($estatus == 1): ?> checked <?php endif; ?> data-toggle="toggle" data-onstyle="success" data-offstyle="danger">

                              </div>

                         </div>
                         <div class="row">
                              <div class="form-group col">
                                   <label>Descripción</label>
                                   <textarea class="form-control" name="descripcion" rows="8" cols="80"><?=$descripcion;?></textarea>
                              </div>
                         </div>

                         <div class="float-right">
                              <button type="submit" class="btn btn-success">Guardar</button>
                              <a href="<?=base_url('admin/proyectos')?>" class="btn bg-gray-500 text-gray-100">Cancelar</a>
                         </div>

                    </form>
               </div>
          </div>
     </div>
</div>
