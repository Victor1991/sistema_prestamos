<div class="row" style="margin-bottom: -30px;">
      <?php if($prestamo['prestamo_padre']):?>
          <div class="form-group col-md-4">
               <label>Monto adeudado prestamo anteriror</label>
               <div class="form-group">
                    <div class="input-group mb-4">
                         <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                         </div>
                         <input <?php if($prestamo['id']):?> readonly <?php endif; ?> class="form-control" required type="text" value="<?=$prestamo['cantidad_anterior']?>" id="monto"  onkeypress="return filterFloat(event,this);">
                    </div>
               </div>
          </div>

          <div class="form-group col-md-4">
              <label>Monto nuevo prestamo</label>
              <div class="form-group">
                   <div class="input-group mb-4">
                         <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                         </div>
                         <input <?php if($prestamo['id']):?> readonly <?php endif; ?> class="form-control" required type="text" value="<?=$prestamo['monto']?>" id="monto"  onkeypress="return filterFloat(event,this);">
                   </div>
              </div>
          </div>

          <div class="form-group col-md-4">
              <label>Monto adeudado</label>
              <div class="form-group">
                   <div class="input-group mb-4">
                         <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                         </div>
                         <input <?php if($prestamo['id']):?> readonly <?php endif; ?> class="form-control" required type="text" value="<?=$prestamo['cantidad_anterior'] + $prestamo['monto'] ?>" id="monto"  onkeypress="return filterFloat(event,this);">
                   </div>
              </div>
          </div>
     <?php else: ?>
          <div class="form-group col-md-4">
              <label>Monto adeudado</label>
              <div class="form-group">
                   <div class="input-group mb-4">
                         <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                         </div>
                         <input <?php if($prestamo['id']):?> readonly <?php endif; ?> class="form-control" required type="text" value="<?=$prestamo['monto']?>" id="monto"  onkeypress="return filterFloat(event,this);">
                   </div>
              </div>
          </div>

     <?php endif; ?>

     <div class="form-group col-md-4">
          <label>Enganche</label>
          <div class="form-group">
               <div class="input-group mb-4">
                    <div class="input-group-prepend">
                         <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                    </div>
                    <input <?php if($prestamo['id']):?> readonly <?php endif; ?> class="form-control" type="text" value="<?=$prestamo['enganche']?>" id="enganche"  onkeypress="return filterFloat(event,this);">
               </div>
          </div>
     </div>

     <div class="form-group col-md-4">
          <label>Tasa interes</label>
          <div class="form-group">
               <div class="input-group mb-4">
                    <div class="input-group-prepend">
                         <span class="input-group-text"><i class="fas fa-percentage"></i></span>
                    </div>
                    <input <?php if($prestamo['id']):?> readonly <?php endif; ?> class="form-control solo-numero" required  value="<?=$prestamo['interes']?>" id="interes"  type="text" name="pin" maxlength="2" >
               </div>
          </div>
     </div>

     <div class="form-group col-md-4">
          <label>Plazo en meses</label>
          <div class="form-group">
               <div class="input-group mb-4">
                    <div class="input-group-prepend">
                         <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                    </div>
                    <input <?php if($prestamo['id']):?> readonly <?php endif; ?> class="form-control solo-numero" required value="<?=$prestamo['meses']?>" id="meses"  type="text" name="pin" maxlength="2" >
               </div>
          </div>
     </div>

     <div class="form-group col-md-4">
          <label>Fecha Inicio</label>
          <div class="form-group">
               <div class="input-group mb-4">
                    <div class="input-group-prepend">
                         <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                    </div>
                    <input <?php if($prestamo['id']):?> readonly <?php endif; ?> class="form-control  <?php if(!$prestamo['id']):?> datepickern<?php endif; ?> " required value="<?=$prestamo['fecha_inicio']?>" id="fecha_inicio"  type="text"  >
               </div>
          </div>
     </div>

     <div class="form-group col-md-4">
          <label>Cuota mensual</label>
          <div class="form-group" style="margin-bottom: -20px;">
               <div class="input-group mb-4">
                    <div class="input-group-prepend">
                         <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                    </div>
                    <input <?php if($prestamo['id']):?> readonly <?php endif; ?> class="form-control solo-numero" required value="<?=$prestamo['cuota_mensual']?>" id="cuota_mensual" type="text" >
               </div>
          </div>

     </div>

</div>
