<div class="row">
     <div class="col-lg-12">
          <!-- Default Card Example -->
          <div class="card shadow mb-4">
               <!-- Card Header - Dropdown -->
               <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary"><?=$titulo_form?></h6>
               </div>
               <!-- Card Body -->
               <div class="card-body">
                    <?php if ($errors): ?>

                         <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                              </button>
                              <p><?= $errors ?></p>
                         </div>

                    <?php endif; ?>

                         <br>
                         <div class="row">
                              <div class="col-md-12">
                                   <a onclick="renovar()" class="btn btn-success btn-icon-split float-right">
                                        <span class="icon text-white-50">
                                             <i class="fas fa-retweet" style="margin-top: 4px;"></i>
                                        </span>
                                        <span class="text">Renovar prestamo</span>
                                   </a>
                              </div>
                         </div>

                         <br>

                         <div class="row">
                              <div class="col-md-12">
                                   <div class="card shadow mb-12">
                                        <!-- Card Header - Accordion -->
                                        <a href="#collapseCardExample" class="d-block card-header py-3
                                        <?php if(count($prestamos_hijo) != 0):?>
                                             collapsed
                                        <?php endif; ?>
                                        " data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
                                             <h6 class="m-0 font-weight-bold text-primary">Prestamo principal</h6>
                                        </a>
                                        <!-- Card Content - Collapse -->
                                        <div class="collapse
                                        <?php if(count($prestamos_hijo) == 0):?>
                                             show
                                        <?php endif; ?>
                                             " id="collapseCardExample" style="">
                                             <div class="card-body">
                                                  <div class="col-md-12">
                                                       <?php $this->load->view('admin/renovacion/info_prestamo'); ?>
                                                       <hr>
                                                       <?php $this->load->view('admin/renovacion/tabla_prestamos', array('prestamo' => $prestamo)); ?>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <?php if(count($prestamos_hijo) > 0):?>

                              <?php foreach ($prestamos_hijo as $key => $prestamo_hijo): ?>
                                   <br>
                                   <div class="row">
                                        <div class="col-md-12">
                                             <div class="card shadow mb-12">
                                                  <!-- Card Header - Accordion -->
                                                  <a href="#collapseCardExample<?=$key?>" class="d-block card-header py-3
                                                  <?php if($prestamo_hijo != end( $prestamos_hijo )): ?>
                                                            collapsed
                                                  <?php endif; ?>
                                                  " data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
                                                       <h6 class="m-0 font-weight-bold text-primary">Renovacion prestamos <?=$key+1?> </h6>
                                                  </a>
                                                  <!-- Card Content - Collapse -->
                                                  <div class="collapse <?php if($prestamo_hijo == end( $prestamos_hijo )): ?>
                                                       show
                                                  <?php endif; ?>" id="collapseCardExample<?=$key?>" style="">
                                                       <div class="card-body">
                                                            <div class="col-md-12">
                                                                 <?php $this->load->view('admin/renovacion/info_prestamo', array('prestamo' => $prestamo_hijo['prestamo'])) ?>

                                                                 <hr>
                                                                 <?php $this->load->view('admin/renovacion/tabla_prestamos', array('tabla_pagos' => $prestamo_hijo['pagos'])) ?>
                                                            </div>
                                                       </div>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              <?php endforeach; ?>
                         <?php endif; ?>
                         <br>
                         <div class="float-right">
                              <a href="<?=base_url('admin/prestamos')?>" class="btn bg-gray-500 text-gray-100">Cancelar</a>
                         </div>


               </div>
          </div>
     </div>
</div>


<div id="modalimagen" class="modal">
     <!-- The Close Button -->
     <span class="close" onclick="$('#modalimagen').modal('hide');">&times;</span>

     <!-- Modal Content (The Image) -->
     <img class="modal-content" id="img01">

     <!-- Modal Caption (Image Text) -->
     <div id="caption"></div>
</div>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="renovar_prestamo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-xl" role="document">
          <form action="<?=base_url('admin/prestamos/prestamos_data')?>" id="form_pago" method="post" enctype="multipart/form-data">
               <div class="modal-content">
                    <div class="modal-body">
                         <h3>Renovar prestamo</h3>
                         <input type="hidden" name="prestamo_id" value="<?=$prestamo['id']?>">
                         <input type="hidden" name="tabla" id="tabla">
                         <input type="hidden" name="cliente_id" value="<?=$prestamo['cliente_id']?>" >
                         <input type="hidden" name="intereses" id="intereses">
                         <input type="hidden" id="creado" value="<?=$prestamo['creado']?>">
                         <input type="hidden" id="ultimo_pago_id"  name="ultimo_pago_id" value="<?=$ultimo_pago_id?>">

                         <div class="row">
                              <div class="col-md-12">
                                   <hr>
                              </div>
                              <div class="col-md-6">
                                   <label>Monto adeudado</label>
                                   <div class="form-group">
                                        <div class="input-group mb-2">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                                             </div>
                                             <input class="form-control action" required="" type="text" value="<?=$adeudo?>" id="modal_monto_adeudo" name="cantidad_anterior" onkeypress="return filterFloat(event,this);">
                                        </div>
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <label for="basic-url">Cantidad a prestar</label>
                                   <div class="form-group">
                                        <div class="input-group mb-2">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                                             </div>
                                             <input class="form-control action" required="" type="text" value="" id="modal_cantidad_prestar" name="monto" onkeypress="return filterFloat(event,this);">
                                        </div>
                                   </div>
                              </div>

                              <div class="col-md-6">
                                   <label for="basic-url">Suma de cantidades</label>
                                   <div class="form-group">
                                        <div class="input-group mb-2">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                                             </div>
                                             <input readonly class="form-control" required="" type="text" value="" id="modal_total" name="total" onkeypress="return filterFloat(event,this);">
                                        </div>
                                   </div>
                              </div>

                              <div class="col-md-6">
                                   <label for="basic-url">Enganche</label>
                                   <div class="form-group">
                                        <div class="input-group mb-2">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                                             </div>
                                             <input class="form-control action" required="" type="text" value="0" id="modal_nuevo_enganche" name="enganche" onkeypress="return filterFloat(event,this);">
                                        </div>
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <label for="basic-url">Tasa interes</label>
                                   <div class="form-group">
                                        <div class="input-group mb-2">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text"><i class="fas fa-percentage"></i></span>
                                             </div>
                                             <input class="form-control action solo-numero" required="" value="17.00" id="modal_interes" name="interes" type="text" maxlength="2">
                                        </div>
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <label for="basic-url">Plazo</label>
                                   <div class="form-group">
                                        <div class="input-group mb-2">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                                             </div>
                                             <input  class="form-control action solo-numero" required="" value="1" id="modal_meses" name="meses" type="text" maxlength="2">
                                        </div>
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <label for="basic-url">Fecha inicio</label>
                                   <div class="form-group">
                                        <div class="input-group mb-2">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                                             </div>
                                             <input  class="form-control action datepickern" required="" value="<?=date('Y-m-d')?>" id="modal_fecha_inicio" name="fecha_inicio" type="text">
                                        </div>
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <label for="basic-url">Cuota mensual</label>
                                   <div class="form-group" style="margin-bottom: -20px;">
                                        <div class="input-group mb-2">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                                             </div>
                                             <input  class="form-control action solo-numero" required="" value="" id="modal_cuota_mensual" name="cuota_mensual" type="text">

                                        </div>
                                        <span id="recomendada"> Cuota mensual recomendada  </span>

                                   </div>
                              </div>
                         </div>
                         <br>
                         <div class="row">
                              <div class="col-md-12">
                                   <hr>
                              </div>
                         </div>
                         <br>
                         <div class="row">
                              <div class="col-md-12">
                                   <div class="table-responsive">
                                        <table class="table" style="font-size:10px;">
                                             <thead class="thead-inverse">
                                                  <tr>
                                                       <th># PAGOS</th>
                                                       <th>CAPITAL AMORTIZADO</th>
                                                       <th>INTERES</th>
                                                       <th>CUOTA</th>
                                                       <th>AMORTIZACION</th>
                                                       <th>MES / AÑO</th>
                                                  </tr>
                                             </thead>
                                             <tbody id="myTable">

                                             </tbody>
                                        </table>
                                   </div>
                              </div>
                         </div>

                    </div>
                    <div class="modal-footer">
                         <button type="submit" class="btn btn-success">Generar</button>
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
               </div>
          </form>
     </div>
</div>

<style>
.m-b-m{
     margin-bottom: 10px;
}
</style>
