<div class="table-responsive">
     <table class="table table-hover table-bordered">
          <thead>
               <tr>
                    <th scope="col"># PAGOS</th>
                    <th scope="col">CAPITAL AMORTIZADO</th>
                    <th scope="col">INTERES</th>
                    <th scope="col">CUOTA</th>
                    <th scope="col">AMORTIZACION</th>
                    <th scope="col">MES / AÑO</th>
                    <th scope="col">PAGOS</th>
               </tr>
          </thead>
          <tbody>
               <?php foreach ($tabla_pagos as $key => $pago): ?>
                    <tr <?php if($pago['renovacion'] == 1): ?> style="color: #fff; background: #53bd97;" <?php endif; ?>>
                         <td>
                              <?=$pago['num'];?>
                         </td>
                         <td>$ <?=number_format($pago['capital'], 2, '.', ',')?></td>
                         <td>$ <?=number_format($pago['interes'], 2, '.', ',')?></td>
                         <td>$ <?=number_format($pago['cuota'], 2, '.', ',')?></td>
                         <td>$ <?=number_format($pago['amortizacion'], 2, '.', ',')?></td>
                         <td><?=$pago['mes/anio']?></td>
                         <td class="text-center">
                              <?php foreach ($pago['pagos'] as $key => $pag): ?>
                                   <h6>
                                        <?php $fecha = strtotime($pag['fecha_pago']) ?>

                                        <span class="badge badge-pill badge-primary">
                                             $ <?=number_format($pag['monto'], 2, '.', ',')?> / <?=$pag['cat_pago']?> / <?= date("d", $fecha)?>-<?= $meses_str[date("n", $fecha)]?>-<?= date("Y", $fecha)?>
                                        </span>
                                   </h6>
                              <?php endforeach; ?>
                              <?php if($pago['renovacion'] == 1): ?>
                                   <span>Renovación de Prestamo</span>
                              <?php endif; ?>

                         </td>
                    </tr>
               <?php endforeach; ?>
          </tbody>
     </table>
</div>
