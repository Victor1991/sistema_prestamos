<div class="row">
     <div class="col-lg-12">
          <!-- Default Card Example -->
          <div class="card shadow mb-4">
               <!-- Card Header - Dropdown -->
               <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary"><?=$titulo_form?></h6>
               </div>
               <!-- Card Body -->
               <div class="card-body">
                    <?php if ($errors): ?>

                         <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                              </button>
                              <p><?= $errors ?></p>
                         </div>

                    <?php endif; ?>


                    <form method="post" action="<?=base_url('admin/prestamos/prestamos_data')?>" id="form_prestamos" enctype="multipart/form-data">

                         <input type="hidden" name="tabla" id="tabla">
                         <input type="hidden" name="intereses" id="intereses">
                         <input type="hidden" name="id" value="<?=$id?>">

                         <div class="row">

                              <div class="form-group col-md-4">
                                   <label>Monto adeudado</label>
                                   <div class="form-group">
                                        <div class="input-group mb-4">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                                             </div>
                                             <input <?php if($id):?> readonly <?php endif; ?> class="form-control" required type="text" value="<?=$monto?>" id="monto" name="monto" onkeypress="return filterFloat(event,this);">
                                        </div>
                                   </div>
                              </div>

                              <div class="form-group col-md-4">
                                   <label>Enganche</label>
                                   <div class="form-group">
                                        <div class="input-group mb-4">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                                             </div>
                                             <input <?php if($id):?> readonly <?php endif; ?> class="form-control" type="text" value="<?=$enganche?>" id="enganche" name="enganche" onkeypress="return filterFloat(event,this);">
                                        </div>
                                   </div>
                              </div>

                              <div class="form-group col-md-4">
                                   <label>Tasa interes</label>
                                   <div class="form-group">
                                        <div class="input-group mb-4">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text"><i class="fas fa-percentage"></i></span>
                                             </div>
                                             <input <?php if($id):?> readonly <?php endif; ?> class="form-control solo-numero" required  value="<?=$interes?>" id="interes" name="interes" type="text" name="pin" maxlength="5" >
                                        </div>
                                   </div>
                              </div>

                              <div class="form-group col-md-4">
                                   <label>Plazo en meses</label>
                                   <div class="form-group">
                                        <div class="input-group mb-4">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                                             </div>
                                             <input <?php if($id):?> readonly <?php endif; ?> class="form-control solo-numero" required value="<?=$meses?>" id="meses" name="meses" type="text" name="pin" maxlength="3" >
                                        </div>
                                   </div>
                              </div>

                              <div class="form-group col-md-4">
                                   <label>Fecha Inicio</label>
                                   <div class="form-group">
                                        <div class="input-group mb-4">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                                             </div>
                                             <input <?php if($id):?> readonly <?php endif; ?> class="form-control  <?php if(!$id):?> datepickern<?php endif; ?> " required value="<?=$fecha_inicio?>" id="fecha_inicio" name="fecha_inicio" type="text"  >
                                        </div>
                                   </div>
                              </div>

                              <div class="form-group col-md-4">
                                   <label>Cuota mensual</label>
                                   <div class="form-group" style="margin-bottom: -20px;">
                                        <div class="input-group mb-4">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                                             </div>
                                             <input <?php if($id):?> readonly <?php endif; ?> class="form-control solo-numero" required value="<?=$cuota_mensual?>" id="cuota_mensual" name="cuota_mensual" type="text" >
                                        </div>
                                   </div>
                                   <span id="recomendada"> Cuota mensual recomendada  </span>
                              </div>

                         </div>

                         <div class="row" style="display:none;">
                              <div class="col-lg-3 m-b-m">
                                   <button type="button" class="btn btn-secondary btn-block">
                                        Tasa interes <br><span class="badge badge-light" id="tasa_interes">  0</span>
                                   </button>
                              </div>
                              <div class="col-lg-3 m-b-m">
                                   <button type="button" class="btn btn-secondary btn-block">
                                        Interes general <br><span class="badge badge-light" id="interes_general"> 0</span>
                                   </button>
                              </div>
                              <div class="col-lg-3 m-b-m">
                                   <button type="button" class="btn btn-secondary btn-block">
                                        Tasa interes anual<br><span class="badge badge-light" id="n"> 0</span>
                                   </button>
                              </div>
                              <div class="col-lg-3 m-b-m">
                                   <button type="button" class="btn btn-secondary btn-block">
                                        Cuota mensual <br><span class="badge badge-light" id="cuota_mensual"> 0</span>
                                   </button>
                              </div>
                         </div>
                         <br>
                         <div class="row">
                              <div class="col-md-12">
                                   <div class="table-responsive">

                                        <table class="table table-hover table-bordered">
                                             <thead>
                                                  <tr>
                                                       <th scope="col"># PAGOS</th>
                                                       <th scope="col">CAPITAL AMORTIZADO</th>
                                                       <th scope="col">INTERES</th>
                                                       <th scope="col">CUOTA</th>
                                                       <th scope="col">AMORTIZACION</th>
                                                       <th scope="col">MES / AÑO</th>
                                                  </tr>
                                             </thead>
                                             <tbody  id="myTable">
                                                  <tr>
                                                       <th colspan="6" class="text-center">SI DATOS A COTIZAR</th>
                                                  </tr>
                                             </tbody>
                                        </table>
                                   </div>
                              </div>
                         </div>
                         <br>
                         <div class="float-right">
                              <?php if(!$id):?>
                                   <button type="submit" class="btn btn-success" id="sub">Guardar</button>
                              <?php endif; ?>
                              <a href="<?=base_url('admin/usuarios')?>" class="btn bg-gray-500 text-gray-100">Cancelar</a>
                         </div>

                    </form>

               </div>
          </div>
     </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal_clientes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
          <div class="modal-content">
               <div class="modal-body">
                    <h3>Cliente a realizar prestamo</h3>
                    <select class="form-control" id="cliente_id">
                         <option value="" disabled selected hidden>Selecciona un cliente</option>
                         <?php foreach ($clientes as $key => $clientes): ?>
                              <option value="<?=$clientes->id?>"  <?php if($clientes->id == $cliente_id): ?> selected <?php endif; ?> >
                                   <?=$clientes->nombres?> <?=$clientes->apellido_paterno?> <?=$clientes->apellido_materno?>
                              </option>
                         <?php endforeach; ?>
                    </select>
               </div>
               <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="seleccionado_cliente()">Generar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
               </div>
          </div>
     </div>
</div>

<style>
.m-b-m{
     margin-bottom: 10px;
}
</style>
