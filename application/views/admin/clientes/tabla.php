<div class="row">

     <div class="col-lg-6">

          <!-- Default Card Example -->
          <div class="card mb-4">
               <div class="card-header">
                    Default Card Example
               </div>
               <div class="card-body">
                    This card uses Bootstrap's default styling with no utility classes added. Global styles are the only things modifying the look and feel of this default card example.
               </div>
          </div>

          <!-- Basic Card Example -->
          <div class="card shadow mb-4">
               <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Basic Card Example</h6>
               </div>
               <div class="card-body">
                    The styling for this basic card example is created by using default Bootstrap utility classes. By using utility classes, the style of the card component can be easily modified with no need for any custom CSS!
               </div>
          </div>

     </div>

     

</div>

<table id="tbl-posts" class="table table-striped">
     <thead>
          <tr>
               <th>Nombre</th>
               <th>Rol</th>
               <th>Correo</th>
               <th>Estatus</th>
               <th class="text-center"><i class="fa fa-cogs"></i></th>
          </tr>
     </thead>
     <tbody>
          <?php foreach ($usuarios as $usuarios): ?>
               <tr>
                    <td>Usuarios</td>

                    <td class="hidden-xs"><?php echo $usuarios->username; ?></td>


                    <td class="hidden-xs"><?php echo $usuarios->role_id; ?></td>
                    <td class="hidden-xs"><?php echo $usuarios->email; ?></td>
                    <td class="hidden-xs"> <td><?=($usuarios->active ? '<span class="label label-success">ON</span>' : '<span class="label label-danger">OFF</span>')?></td></td>
                    <td style="text-align: center;">
                         <a href="<?= base_url('admin/usuarios/form/' . $usuarios->id) ?>" class="btn btn-info btn-sm" title="Editar"><i class="fa fa-edit"></i></a>
                         <a href="#" id="usu_<?=$usuarios->id?>" class="btn btn-danger btn-sm btn-delete" title="Eliminar" onclick="Eliminar(this)"><i class="fa fa-times"></i></a>
                    </td>
               </tr>
          <?php endforeach; ?>
     </tbody>
     <tfoot>
          <tr>
               <td colspan="8"><?php echo $pagination['links']; ?></td>
          </tr>
     </tfoot>
</table>
