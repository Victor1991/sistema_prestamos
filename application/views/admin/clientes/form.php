<div class="row">
     <div class="col-lg-12">
          <!-- Default Card Example -->
          <div class="card shadow mb-4">
               <!-- Card Header - Dropdown -->
               <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary"><?=$titulo_form?></h6>
               </div>
               <!-- Card Body -->
               <div class="card-body">
                    <?php if ($errors): ?>

                         <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                              </button>
                              <p><?= $errors ?></p>
                         </div>

                    <?php endif; ?>


                    <form method="post" action="" id="myCoolForm" enctype="multipart/form-data">


                         <div class="row">
                              <div class="form-group col">
                                   <label>Nombres</label>
                                   <input type="text" name="nombres" class="form-control" value="<?=$nombres;?>">
                              </div>

                              <div class="form-group col">
                                   <label>Apellido paterno</label>
                                   <input type="text" name="apellido_paterno" class="form-control" value="<?=$apellido_paterno;?>">

                              </div>

                              <div class="form-group col">
                                   <label>Apellido materno</label>
                                   <input type="text" name="apellido_materno" class="form-control" value="<?=$apellido_materno;?>">

                              </div>

                         </div>
                         <div class="row">
                              <div class="form-group col">
                                   <label>Teléfono</label>
                                   <input type="text" name="telefono" class="form-control" value="<?=$telefono;?>">
                              </div>

                              <div class="form-group col">
                                   <label>Celular</label>
                                   <input type="text" name="celular" class="form-control" value="<?=$celular;?>">
                              </div>

                              <div class="col">
                                   <label>Tipo de cliente</label>
                                   <div class="col che_borde">
                                        <div class="form-check form-check-inline">
                                             <input class="form-check-input" name="cliente" type="checkbox" value="1" <?php if($cliente == 1): ?> checked <?php endif;  ?> >
                                             <label class="form-check-label" for="inlineCheckbox1">Cliente</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                             <input class="form-check-input" name="inversinista" type="checkbox" value="1" <?php if($inversinista == 1): ?> checked <?php  endif; ?>>
                                             <label class="form-check-label" for="inlineCheckbox2">Inversionista</label>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <div class="row">
                              <div class="form-group col">
                                   <label>Dirección</label>
                                   <input type="text" name="direccion" class="form-control" value="<?=$nombres;?>">
                              </div>
                         </div>


                         <div class="row">
                              <div class="form-group col-md-6">
                                   <label>INE</label><br>
                                   <input type="file" accept="image/*" onchange="loadFile(event)" name="ine"><br><br>
                                   <img id="ine" src="<?=$ine ? base_url('assets/files/').$ine : base_url('assets/img/no-image.jpg')?>" class="img-thumbnail"/>

                              </div>
                              <div class="form-group col-md-6">
                                   <label>Comprobante domicilio.</label><br>
                                   <input type="file" accept="image/*" onchange="loadFile(event)" name="comprobante_domicilio"><br><br>
                                   <img id="comprobante_domicilio" src="<?=$comprobante_domicilio ? base_url('assets/files/').$comprobante_domicilio : base_url('assets/img/no-image.jpg')?>" class="img-thumbnail"/>
                              </div>
                         </div>





                         <div class="float-right">
                              <button type="submit" class="btn btn-success">Guardar</button>
                              <a href="<?=base_url('admin/usuarios')?>" class="btn bg-gray-500 text-gray-100">Cancelar</a>
                         </div>

                    </form>
               </div>
          </div>
     </div>
</div>
