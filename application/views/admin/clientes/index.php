<div class="row">
     <div class="col-lg-12">
          <!-- Default Card Example -->
          <div class="card shadow mb-4">
               <!-- Card Header - Dropdown -->
               <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Listado de Clientes</h6>
                    <a href="<?= base_url('admin/clientes/form') ?>" class="btn btn-success" style="color: #FFFFFF;">

                         <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Clientes
                    </a>
               </div>
               <!-- Card Body -->
               <div class="card-body">
                    <?php if ($errors): ?>
                         <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                              </button>
                              <p><?= $errors ?></p>
                         </div>
                    <?php endif; ?>

                    <?php if ($messages): ?>
                         <div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                              </button>
                              <p><?= $messages ?></p>
                         </div>
                    <?php endif; ?>

                    <div class="table-responsive" style="border: 0px !important;">
                         <div>
                              <form method="get" action="<?php echo base_url('admin/usuarios'); ?>" class="row" style="width:100%">
                                   <div class="col-sm-4">
                                        <div class="form-group">
                                             <label>Buscar:</label>
                                             <input type="text" name="username" id="username" class="form-control" onkeypress="filtro()">
                                        </div>
                                   </div>

                                   <div class="col-sm-2">
                                        <div class="form-group m-t-25">
                                             <label> &nbsp;</label>
                                             <a id="f_btn_cancelar" class="btn btn-warning btn-block cancel" onclick="Cancelar()">Cancelar</a>
                                        </div>
                                   </div>
                              </form>

                         </div>

                         <hr>

                         <!--  -->
                         <div id="filter-table-data">
                              <table id="tbl-posts" class="table table-striped table-bordered table-hover dataTables-users" >
                                   <thead>
                                        <tr>
                                             <th >Nombre</th>
                                             <th >Teléfono</th>
                                             <th >Celular</th>
                                             <th >Tipo de cliente</th>
                                             <th class="text-center"><i class="fa fa-cogs"></i></th>
                                        </tr>
                                   </thead>
                                   <tbody id="tcontent">
                                   </tbody>
                              </table>
                              <div id="page">
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>

     <!-- MOdal -->
     <div class="modal fade-scale" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h4 class="modal-title" id="myModalLabel"><span id="title-modal">Información usuario</span></h4>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>

                    <div class="modal-body">
                         <form class="form-horizontal" id="form-proyectos">
                              <div class="form-group">
                                   <label class="col-lg-12 col-md-12 control-label">Nombre</label>
                                   <div class="col-lg-12 col-md-12">
                                        <label class="control-label" id="nombre_modal"></label>
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="col-lg-12 col-md-12 control-label">Rol</label>
                                   <div class="col-lg-12 col-md-12">
                                        <label class="control-label"  id="rol_modal"></label>
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="col-lg-12 col-md-12 control-label">Estatus</label>
                                   <div class="col-lg-12 col-md-12">
                                        <label class="control-label"  id="estatus_modal"></label>
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="col-lg-12 col-md-12 control-label">Correo</label>
                                   <div class="col-lg-12 col-md-12">
                                        <label class="control-label" id="correo_modal"></label>
                                   </div>
                              </div>
                         </form>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="btn btn-default bg-gray-500 text-gray-100" data-dismiss="modal" onclick="borrar_fomm();">Cerrar</button>
                    </div>
               </div>
          </div>
     </div>

</div>
