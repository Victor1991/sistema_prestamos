                    </div>
                    <!-- /.container-fluid -->

               </div>
               <!-- End of Main Content -->

               <!-- Footer -->
               <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                         <div class="copyright text-center my-auto">
                              <span>Suma Web &copy; <?=date('Y');?></span>
                         </div>
                    </div>
               </footer>
               <!-- End of Footer -->

          </div>
          <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
     <i class="fas fa-angle-up"></i>
</a>

<!-- Bootstrap core JavaScript-->
<script src="<?=base_url()?>vendor/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?=base_url()?>vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?=base_url()?>assets/js/sb-admin-2.js"></script>

<!-- Page level plugins -->
<script src="<?=base_url()?>vendor/chart.js/Chart.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.js"> </script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js"> </script>

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>


<script src="https://code.highcharts.com/highcharts.js"></script>


<script>
    var baseURL = '<?= base_url() ?>';
</script>

<?php if (isset($this->assets['js'])): ?>
    <?php foreach ($this->assets['js'] as $js) : ?>
        <script src="<?php echo base_url('/assets/' . trim($js, '/')).'?random='.rand(); ?>"></script>
    <?php endforeach; ?>
<?php endif; ?>


</body>
</html>
