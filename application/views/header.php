<!DOCTYPE html>
<html lang="en">

<head>

     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <meta name="description" content="">
     <meta name="author" content="">

     <title>Presatamos</title>

     <!-- Custom fonts for this template-->
     <link href="<?=base_url()?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
     <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

     <!-- Custom styles for this template-->
     <link href="<?=base_url()?>assets/css/sb-admin-2.css" rel="stylesheet">

     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">

     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.css">

     <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

</head>
<div id="load" style="width:100%; height:100%; position: absolute; z-index: 9999; background-color: #ffffffc7; text-align:center; display: none;">
     <img src="<?=base_url('/assets/img/load2.gif')?>" alt="" style="margin-top:10%;">
</div>

<body id="page-top">

     <!-- Page Wrapper -->
     <div id="wrapper">

          <!-- Sidebar -->
          <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

               <!-- Sidebar - Brand -->
               <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                    <div class="sidebar-brand-icon rotate-n-15">
                         <i class="fab fa-connectdevelop"></i>
                    </div>
                    <div class="sidebar-brand-text mx-3">Prestamos</div>
               </a>

               <!-- Divider -->
               <hr class="sidebar-divider my-0">


               <!-- Nav Item - Dashboard -->
               <li class="nav-item <?php if($this->seccion == 1):?> active  <?php endif;?> ">
                    <a class="nav-link" href="<?= base_url('admin/') ?>">
                         <i class="fas fa-fw fa-tachometer-alt"></i>
                         <span>Dashboard</span>
                    </a>
               </li>

               <!-- Divider -->
               <hr class="sidebar-divider my-0">

               <!-- Nav Item - Pages Collapse Menu -->
               <li class="nav-item <?php if($this->seccion == 2):?> active  <?php endif;?>">
                    <a class="nav-link" href="<?= base_url('admin/usuarios') ?>">
                         <i class="fas fa-users"></i>
                         <span>Usuarios</span></a>
                    </li>
               </li>

               <hr class="sidebar-divider my-0">

               <!-- Nav Item - Pages Collapse Menu -->
               <li class="nav-item <?php if($this->seccion == 3):?> active  <?php endif;?>">
                    <a class="nav-link" href="<?= base_url('admin/clientes') ?>">
                         <i class="far fa-grin"></i>
                         <span>Clientes</span></a>
                    </li>
               </li>

               <!-- Divider -->
               <hr class="sidebar-divider my-0">


               <!-- Nav Item - Pages Collapse Menu -->
               <li class="nav-item <?php if($this->seccion == 4):?> active  <?php endif;?>">
                    <a class="nav-link" href="<?= base_url('admin/prestamos') ?>">
                         <i class="fas fa-wallet"></i>
                         <span>Prestamos</span></a>
                    </li>
               </li>

               <hr class="sidebar-divider my-0">


               <!-- Nav Item - Pages Collapse Menu -->
               <li class="nav-item <?php if($this->seccion == 5):?> active  <?php endif;?>">
                    <a class="nav-link" href="<?= base_url('admin/pagos') ?>">
                         <i class="fas fa-file-invoice-dollar"></i>
                         <span>Pagos</span></a>
                    </li>
               </li>


               <hr class="sidebar-divider my-0">


               <!-- Nav Item - Pages Collapse Menu -->
               <li class="nav-item <?php if($this->seccion == 6):?> active  <?php endif;?>">
                    <a class="nav-link" href="<?= base_url('admin/proyectos') ?>">
                         <i class="fas fa-briefcase"></i>
                         <span>Proyectos</span></a>
                    </li>
               </li>


               <hr class="sidebar-divider my-0">


               <!-- Nav Item - Pages Collapse Menu -->
               <li class="nav-item <?php if($this->seccion == 7):?> active  <?php endif;?>">
                    <a class="nav-link" href="<?= base_url('admin/Inversionistas') ?>">
                         <i class="fas fa-chart-line"></i>
                         <span>Inversionistas</span></a>
                    </li>
               </li>


               <!-- Divider -->
               <hr class="sidebar-divider">

               <!-- Sidebar Toggler (Sidebar) -->
               <div class="text-center d-none d-md-inline">
                    <button class="rounded-circle border-0" id="sidebarToggle"></button>
               </div>


          </ul>
          <!-- End of Sidebar -->

          <!-- Content Wrapper -->
          <div id="content-wrapper" class="d-flex flex-column">

               <!-- Main Content -->
               <div id="content">

                    <!-- Topbar -->
                    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                         <!-- Sidebar Toggle (Topbar) -->
                         <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                              <i class="fa fa-bars"></i>
                         </button>


                         <!-- Topbar Navbar -->
                         <ul class="navbar-nav ml-auto">

                              <div class="topbar-divider d-none d-sm-block"></div>

                              <!-- Nav Item - User Information -->
                              <li class="nav-item dropdown no-arrow">
                                   <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                                             <?=$_SESSION['usuario_nombres']?> <?=$_SESSION['usuario_apellido_paterno']?> <?=$_SESSION['usuario_apellido_materno']?>
                                        </span>
                                        <img class="img-profile rounded-circle" src="http://www.jdevoto.cl/web/wp-content/uploads/2018/04/default-user-img.jpg">
                                   </a>
                                   <!-- Dropdown - User Information -->
                                   <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                        <a class="dropdown-item" href="<?=base_url('admin/usuarios/form/').$_SESSION['usuario_id']?>">
                                             <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                             Perfil
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="<?= base_url('login/logout') ?>" >
                                             <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                             Salir
                                        </a>
                                   </div>
                              </li>

                         </ul>

                    </nav>
                    <!-- End of Topbar -->
                    <!-- Begin Page Content -->
                    <div class="container-fluid">

                         <!-- Page Heading -->
                         <div class="d-sm-flex align-items-center justify-content-between mb-4">
                              <h1 class="h3 mb-0 text-gray-800"><?=$this->nombre_seccion?></h1>
                              <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
                         </div>
