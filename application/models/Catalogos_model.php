<?php
class Catalogos_model extends CI_Model {

     public function get_tipo_pago(){
          return $this->db->get('catalogo_tipo_pagos')->result();
     }

     public function get_tipo_aportacion(){
          return $this->db->get('catalogo_aportaciones')->result();
     }
}
