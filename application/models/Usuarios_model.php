<?php
class Usuarios_model extends CI_Model {
    private $_table = 'usuarios';

    public function get_all_admin() {
        $this->db->select('id, username as nombre, role_id as rol, email, active as estatus')
                ->from('usuarios');
            return $this->db->get()->result();
    }

    public function get_by_id($id)
    {
        $query = $this->db->select("usuarios.*, username as nombre_completo", FALSE)
                ->from("usuarios")
                ->where("id", $id)
                ->get();

        return $query->row();
    }

    public function get_all() {


        $posts = $this->db->get($this->_table)->result();

        // echo "===========";
        // echo ($this->db->last_query());
        // echo "================<br>";
        return $posts;
    }


    public function get_roles($role_id = 0)
    {
        $this->db->select('*')
                ->from("roles")
                ->where("id >=", $role_id);
                return $this->db->get()->result();
        //return $this->db->order_by('id', 'asc')->get('roles')->result();
    }

    public function get_usuario_by_id($id_usuario)
    {
    	$this->db->select('*')
    			->from('usuarios')
    			->where('id', $id_usuario);
    			  return $this->db->get()->row();
    }

    public function insert_usuario($save){
        $this->db->insert('usuarios', $save);
        return $this->db->insert_id();
    }

    public function update_usuario($save, $usuario_id)
    {
         return $this->db->where('id', $usuario_id)->update('usuarios', $save);
    }

    public function delete_usuario($usuario_id)
    {
        return $this->db->where('id', $usuario_id)->delete('usuarios');
    }



    public function num_usuarios($params = array()) {
        $this->db->from('usuarios');

        //rol
        if (isset($params['role_id'])) {
               $this->db->where('role_id', $params['role_id']);
        }

        //username
        if (isset($params['username'])) {
              $this->db->like("username", $params['username']);
        }

        return $this->db->count_all_results();
    }

     public function get_many($params = array(), $orden = NULL, $limit = 0, $start = 0) {
       $this->_get_all_setup();


        if (isset($params['role_id'])) {
               $this->db->where('role_id', $params['role_id']);
        }

        //username
        if (isset($params['username'])) {
              $this->db->like("username", $params['username']);
        }

         if (!is_null($orden) && is_array($orden)) {
            foreach ($orden as $key => $value) {
                $this->db->order_by($orden['columna'], $orden['orden']);
            }
        }

       $this->db->limit($limit, $start);



        return $this->get_all();
    }


     private function _get_all_setup() {
        $this->_table = NULL;
        $this->db->select("usu.*, CONCAT(usu.nombres, ' ', usu.apellido_paterno ) as nombre_completo");
        $this->db->select("ro.name as rol");
        $this->db->select("CONCAT(usu.id) as usuarios", FALSE);
        $this->db->from('usuarios usu');
        $this->db->join(' roles ro', 'usu.role_id = ro.id', 'inner');
    }


    public function info_usuario_id($usuario_id)
    {
        $this->db->select("CONCAT(usu.nombres, ' ', usu.apellido_paterno ) as nombre_completo, ro.name, usu.active, usu.email");
        $this->db->select("ro.name as rol");
        $this->db->from('usuarios usu');
        $this->db->join(' roles ro', 'usu.role_id = ro.id', 'inner');
        $this->db->where('usu.id', $usuario_id);
        return $this->db->get()->result();
    }


}
