<?php

class Auth_model extends CI_Model {

    protected $errores;
    protected $contar_intentos_login;
    protected $contar_intentos_ip;
    protected $cant_max_intentos;
    protected $tiempo_bloqueo;
    protected $codigo_pwd_caduca;
    protected $error_tag_start;
    protected $error_tag_end;


    public function __construct() {
        parent::__construct();

        $this->lang->load('auth');

        $this->errores = array();

        //configuración
        $this->contar_intentos_login = TRUE;
        $this->contar_intentos_ip = FALSE;
        $this->cant_max_intentos = 5;
        $this->tiempo_bloqueo = 300; //tiempo (segundos) que la cuenta es bloqueada
        $this->codigo_pwd_caduca = 60 * 60 * 60; //tiempo (segundos) que tarda en caducar el código para recuperar la contraseña

        //tags inicio y fin para los mensajes
        $this->error_tag_start = '<p>';
        $this->error_tag_end = '</p>';

        /*
        | Bcrypt is available in PHP 5.3+
        |
        | NOTE: If you use bcrypt you will need to increase your password column character limit to (80)
        |
        | Below there is "default_rounds" setting.  This defines how strong the encryption will be,
        | but remember the more rounds you set the longer it will take to hash (CPU usage) So adjust
        | this based on your server hardware.
        |
        | If you are using Bcrypt the Admin password field also needs to be changed in order to login as admin:
        | $2y$: $2y$08$200Z6ZZbp3RAEXoaWcMA6uJOFicwNZaqk4oDhqTUiFXFe63MG.Daa
        | $2a$: $2a$08$6TTcWD1CJ8pzDy.2U3mdi.tpl.nYOR1pwYXwblZdyQd9SL16B7Cqa
        |
        | Be careful how high you set max_rounds, I would do your own testing on how long it takes
        | to encrypt with x rounds.
        |
        | salt_prefix: Used for bcrypt. Versions of PHP before 5.3.7 only support "$2a$" as the salt prefix
        | Versions 5.3.7 or greater should use the default of "$2y$".
         */

        $params = array(
            'rounds' => 8,
            'salt_prefix' => version_compare(PHP_VERSION, '5.3.7', '<') ? '$2a$' : '$2y$',
            );
        $this->load->library('bcrypt', $params);
    }

    public function login($username, $password) {

        if (empty($username) || empty($password)) {
            $this->set_error('login_incorrecto');
            return FALSE;
        }

        $query = $this->db->select('*')
        ->from('usuarios')
        ->where('username', $username)
        ->limit(1)
        ->order_by('id', 'desc')
        ->get();



        //if ($this->usuario_bloqueado($username)) {
        //     $this->agregar_intento_login($username);
        //     $this->set_error('login_bloqueado');
        //             return FALSE;
        //}

        if ($query->num_rows() === 1) {
            $usuario = $query->row();
            if ($this->bcrypt->verify($password, $usuario->password_hash)) {
                if ($usuario->active == 0) {
                  //  $this->agregar_intento_login($username);
                    $this->set_error('login_cuenta_no_activa');

                    return FALSE;
                }
                $this->set_session($usuario);

                return TRUE;
            }
        }



       // $this->agregar_intento_login($username);
        $this->set_error('login_incorrecto');

        return FALSE;
    }

    /**
     * Obtener usuario por su email
     * @param string $email
     * @return object
     */
    public function get_usuario_by_email($email) {
        return $this->db->where('email', $email)->limit(1)->get('usuarios')->row();
    }

    /**
     * Obtener usuario por su username
     * @param string $username
     * @return object
     */
    public function get_usuario_by_username($username) {
        return $this->db->where('username', $username)->limit(1)->get('usuarios')->row();
    }

    /**
     * Crear variables de sesión
     *
     * @param object $usuario
     * @return boolean
     */
    public function set_session($usuario) {

        $session_data = array(
            'usuario_username' => $usuario->username,
            'usuario_nombres' => $usuario->nombres ,
            'usuario_apellido_paterno' => $usuario->apellido_paterno ,
            'usuario_apellido_materno' => $usuario->apellido_materno ,
            'usuario_email' => $usuario->email,
            'usuario_id' => $usuario->id,
            'rol_id' => $usuario->role_id,
            //'rol_nombre' => $usuario->rol->username,
            'ultimo_login' => $usuario->last_login,
            'logged_in' => TRUE
            );

        $this->session->set_userdata($session_data);

        return TRUE;
    }

    /**
     * Crear hash del password
     *
     * @param string $password
     * @return boolean
     */
    public function hash_password($password) {
        if (empty($password)) {
            return FALSE;
        }

        // bcrypt
        return $this->bcrypt->hash($password);
    }

    /**
     * Se determina si una cuenta debe ser bloqueada debido a los intentos
     * de inicio de sesión erroneos, dentro de un período de tiempo
     *
     * @return	boolean
     */
    public function usuario_bloqueado($username) {
        return $this->intento_logins_excedido($username) /*&& $this->get_ultimo_intento_login($username) > time() - $this->tiempo_bloqueo*/;
    }

    /**
     * Obtiene el tiempo del último intento de acceso al sistema,
     * por ip o por username de acuerdo a lo indicado en la configuración
     *
     * @param string $username
     * @return int
     */
    public function get_ultimo_intento_login($username) {
        if ($this->contar_intentos_login) {
            $ip_address = $this->input->ip_address();
            $this->db->select_max('time');

            if ($this->contar_intentos_ip) {
                $this->db->where('ip_address', $ip_address);
            } else if (strlen($username) > 0) {
                $this->db->or_where('username', $username);
            }

            $res = $this->db->get('intentos_login', 1);

            if ($res->num_rows() > 0) {
                return $res->row()->time;
            }
        }

        return 0;
    }

    /**
     * Determina si se han alcanzado el número máx. de intentos, si así se
     * indico en la opción de contar los intentos de login
     *
     * @param string $username
     * @return boolean
     */
    public function intento_logins_excedido($username) {
        if ($this->contar_intentos_login && $this->cant_max_intentos > 0) {
            $intentos = $this->get_cant_intentos_login($username);
            return $intentos >= $this->cant_max_intentos;
        }
        return FALSE;
    }

    /**
     * Cantidad de intentos de login fallidos
     *
     * Según la configuración cuenta por ip y/o username
     *
     * @param string $username
     * @return int
     */
    public function get_cant_intentos_login($username) {
        if ($this->contar_intentos_login) {
            $ip_address = $this->input->ip_address();
            $this->db->select('1', FALSE);
            if ($this->contar_intentos_ip) {
                $this->db->where('ip_address', $ip_address);
                $this->db->where('username', $username);
            } else if (strlen($username) > 0) {
                $this->db->where('username', $username);
            }
            $res = $this->db->get('intentos_login');
            return $res->num_rows();
        }
        return 0;
    }

    /**
     * Agregar intento de login fallido
     *
     * @param string $username
     * @return boolean
     */
    public function agregar_intento_login($username) {
        if ($this->contar_intentos_login) {
            $ip_address = $this->input->ip_address();
            return $this->db->insert('intentos_login', array('username' => $username, 'ip_address' => $ip_address, 'time' => time()));
        }
        return FALSE;
    }

    /**
     * Borrar los intentos de login pasados
     *
     * @param string $username
     * @param int $tiempo_expiracion
     * @return boolean
     */
    public function reset_intentos_login($username, $tiempo_expiracion = 86400) {
        if ($this->contar_intentos_login) {
            $ip_address = $this->input->ip_address();

            $this->db->where(array('ip_address' => $ip_address, 'username' => $username));
            // eliminar intentos viejos
            $this->db->or_where('time <', time() - $tiempo_expiracion, FALSE);

            return $this->db->delete('intentos_login');
        }

        return FALSE;
    }

    /**
     * Actualizar último login
     *
     * @param int $id
     * @return boolean
     */
    public function actualizar_ultimo_login($id) {
        $this->db->update('usuarios', array('ultimo_login' => date('Y-m-d H:i:s')), array('id' => $id));

        return $this->db->affected_rows() == 1;
    }

    /**
     * Obtiene el keyword del rol de un usuario
     *
     * @param int $usurio
     * @return boolean
     */
    public function get_rol($usurio) {
        $query = $this->db->select('roles.clave')
        ->from('usuarios')
        ->join('roles', 'roles.id=usuarios.rol_id', 'join')
        ->where('usuarios.id', $usurio)
        ->limit(1)
        ->get();

        if ($query->num_rows() == 1) {
            return $query->row()->clave;
        }

        return FALSE;
    }

    /**
     * Verifica si existe el email
     *
     * @param string $email
     * @return boolean
     */
    public function existe_email($email) {
        $query = $this->db->select('id')
        ->from('usuarios')
        ->where('email', $email)
        ->get();

        if ($query->num_rows() > 0) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Verifica si existe el username
     *
     * @param string $username
     * @return boolean
     */
    public function existe_username($username) {
        $query = $this->db->select('id')
        ->from('usuarios')
        ->where('username', $username)
        ->get();

        if ($query->num_rows() > 0) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * set_error
     *
     * Añadir errores
     *
     * @param string $error
     * @return void
     */
    public function set_error($error) {
        $this->errores[] = $error;
    }

    /**
     * errors
     *
     * regresa los errores generados
     *
     * @return void
     * */
    public function errores() {
        $_output = '';
        foreach ($this->errores as $error) {
            $error_str = $this->lang->line($error) ? $this->lang->line($error) : '##' . $error . '##';
            $_output .= $this->error_tag_start . $error_str . $this->error_tag_end;
        }

        return $_output;
    }

    /**
     * Genera y asigna un código para poder recuperar la contraseña
     *
     * @param string $email
     * @return boolean
     */
    public function generar_codigo_recuperar_pwd($email) {
        if (empty($email)) {
            return false;
        }

        $codigo = md5(microtime() . $email);
        $data['codigo_recuperar_pwd'] = $codigo;
        $data['time_recuperar_pwd'] = time();
        $this->db->update('usuarios', $data, array('email' => $email));
        return $this->db->affected_rows() == 1;
    }

    /**
     * Valida si el código y el username coinciden así como que el código
     * aún no haya caducado.
     *
     * @param string $username
     * @param string $codigo
     * @return boolean
     */
    public function validar_codigo_cambiar_pass($username, $codigo) {
        $usuario = $this->db->where('username', $username)
        ->where('codigo_recuperar_pwd', $codigo)
        ->get('usuarios')->row();

        if ($usuario) {
            if ($this->_caduca_codigo($usuario->time_recuperar_pwd)) {
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }

    /**
     * verifica si ha trascurrido el tiempo para que el código sea valido
     *
     * @param int $tiempo
     * @return boolean
     */
    private function _caduca_codigo($tiempo) {
        //time() - 3600
        $t_transcurrido = time() - $tiempo;
        return $t_transcurrido > $this->codigo_pwd_caduca;
    }

    /**
     * Cambia la contraseña del usuario $username
     *
     * @param string $username
     * @param string $password
     * @return boolean
     */
    public function cambiar_password($username, $password) {
        $this->db->update('usuarios', array('password_hashed' => $this->hash_password($password)), array('username' => $username));
        return $this->db->affected_rows() == 1;
    }

    /**
     *
     * regresa el arreglo de mensajes de error
     * @param boolean $langify Determina si se regresa la cadena de error o el key de error
     * @return array
     * */
    public function errors_array($langify = TRUE) {
        if ($langify) {
            $_output = array();
            foreach ($this->errores as $error) {
                $error_str = $this->lang->line($error) ? $this->lang->line($error) : '##' . $error . '##';
                $_output[] = $this->error_tag_start . $error_str . $this->error_tag_end;
            }
            return $_output;
        } else {
            return $this->errores;
        }
    }

    /**
     * Vaciar los mensajes de error
     *
     * @return void
     * */
    public function reset_errores() {
        $this->errores = array();

        return TRUE;
    }

}
