<?php
/**
* @VictorHugo
* 02/01/2020
*/
class Proyectos_model extends CI_Model{
     private $_table = 'proyectos';

     public function insert($save){
          $this->db->insert($this->_table, $save);
          return $this->db->insert_id();
     }

     public function update($id, $save){
          $this->db->where('id', $id);
          return $this->db->update($this->_table, $save);
     }

     public function get_by_id($id){
          $this->db->where('id', $id);
          return $this->db->get($this->_table)->row();
     }

     public function params($params = array()){
          if (isset($params['nombre'])) {
               $this->db->like("nombre", $params['nombre']);
          }
     }

     public function count_all($params = array()){

          $this->params($params);
          $this->db->from($this->_table);
          return $this->db->count_all_results();
     }

     public function get_proyects(){
            return $this->db->get($this->_table)->result();
     }

     public function get_all($params = array(), $orden = NULL, $limit = 0, $start = 0){
          $this->params($params);

          if (!is_null($orden) && is_array($orden)) {
               $this->db->order_by($orden['columna'], $orden['orden']);
         }

        $this->db->limit($limit, $start);
        return $this->db->get($this->_table)->result();
     }

}

?>
