<?php
/**
* @VictorHugo
* 13/01/2020
*/
class Aportaciones_model extends CI_Model{
     private $_table = 'aporataciones_proyecto';

     public function insert($save){
          $this->db->insert($this->_table, $save);
          return $this->db->insert_id();
     }

     public function update($id, $save){
          $this->db->where('id', $id);
          return $this->db->update($this->_table, $save);
     }

     public function get_by_id($id){
          $this->db->where('id', $id);
          return $this->db->get($this->_table)->row();
     }

     public function params($params = array()){
          if (isset($params['nombre'])) {
               $this->db->like("nombre", $params['nombre']);
          }
     }

     public function count_all($params = array()){

          $this->params($params);
          $this->db->from($this->_table);
          return $this->db->count_all_results();
     }

     public function get_proyects(){
            return $this->db->get($this->_table)->result();
     }

     public function get_all($params = array(), $orden = NULL, $limit = 0, $start = 0){
          $this->params($params);

          if (!is_null($orden) && is_array($orden)) {
               $this->db->order_by($orden['columna'], $orden['orden']);
         }

        $this->db->limit($limit, $start);
        return $this->db->get($this->_table)->result();
     }

     public function get_aportaciones_cliente($cliente_id = false){
          $this->db->select("proyectos.nombre as nom_proyecto,
                              proyectos.interes, proyectos.estatus,
                              proyectos.interes, proyectos.descripcion,
                              clientes.nombres as nombre,
                              clientes.apellido_paterno,
                              clientes.apellido_materno,
                              clientes.apellido_materno,
                              SUM(CASE WHEN aporataciones_proyecto.`cat_aport_id` = 1 THEN aporataciones_proyecto.`cantidad` ELSE 0 END) AS aportacion,
                              SUM(CASE WHEN aporataciones_proyecto.`cat_aport_id` = 2 THEN aporataciones_proyecto.`cantidad` ELSE 0 END) AS pago,
                              ".$this->_table.".*");
          $this->db->from($this->_table);
          $this->db->join('proyectos', 'proyectos.id = '.$this->_table.'.proyecto_id', 'left' );
          $this->db->join('clientes', 'clientes.id = '.$this->_table.'.usuario_id', 'left' );
          $this->db->join('catalogo_tipo_pagos', 'catalogo_tipo_pagos.id = '.$this->_table.'.cat_pago_id', 'left' );
          $this->db->join('catalogo_aportaciones', 'catalogo_aportaciones.id = '.$this->_table.'.cat_aport_id', 'left' );
          if ($cliente_id) {
               $this->db->where($this->_table.'.usuario_id',$cliente_id );
          }
          $this->db->group_by('proyectos.id');
          return $this->db->get()->result();
     }

}

?>
