<?php
class Clientes_model extends CI_Model {
    private $_table = 'clientes';

    public function get_all_admin() {
        $this->db->select('id, username as nombre, role_id as rol, email, active as estatus')
                ->from('clientes');
            return $this->db->get()->result();
    }

    public function get_by_id($id)
    {
        $query = $this->db->select("*", FALSE)
                ->from("clientes")
                ->where("id", $id)
                ->get();

        return $query->row();
    }

    public function get_all() {


        $posts = $this->db->get($this->_table)->result();

        // echo "===========";
        // echo ($this->db->last_query());
        // echo "================<br>";
        return $posts;
    }


    public function get_roles($role_id = 0)
    {
        $this->db->select('*')
                ->from("clientes")
                ->where("id >=", $role_id);
                return $this->db->get()->result();
        //return $this->db->order_by('id', 'asc')->get('roles')->result();
    }

    public function get_usuario_by_id($id_usuario)
    {
    	$this->db->select('*')
    			->from('clientes')
    			->where('id', $id_usuario);
    			  return $this->db->get()->row();
    }

    public function insert($save){
        $this->db->insert('clientes', $save);
        return $this->db->insert_id();
    }

    public function update($save, $usuario_id)
    {
         return $this->db->where('id', $usuario_id)->update('clientes', $save);
    }

    public function delete($usuario_id)
    {
        return $this->db->where('id', $usuario_id)->delete('clientes');
    }



    public function num_clientes($params = array()) {
        $this->db->from('clientes');

          if (isset($params['inversinista'])) {
               $this->db->where('inversinista', 1);
          }

        //username
        if (isset($params['username'])) {
             $this->db->like("nombres", $params['username']);
             $this->db->or_like("apellido_paterno", $params['username']);
             $this->db->or_like("apellido_materno", $params['username']);
        }

        return $this->db->count_all_results();
    }

     public function get_many($params = array(), $orden = NULL, $limit = 0, $start = 0) {
       $this->_get_all_setup();

       if (isset($params['inversinista'])) {
            $this->db->where('inversinista', 1);
       }

        //username
        if (isset($params['username'])) {
              $this->db->like("nombres", $params['username']);
               $this->db->or_like("apellido_paterno", $params['username']);
              $this->db->or_like("apellido_materno", $params['username']);
        }

         if (!is_null($orden) && is_array($orden)) {
            foreach ($orden as $key => $value) {
                $this->db->order_by($orden['columna'], $orden['orden']);
            }
        }

       $this->db->limit($limit, $start);

        return $this->get_all();
    }


     private function _get_all_setup() {
        $this->_table = NULL;
        $this->db->select("*");
        $this->db->from('clientes');
    }


    public function info_usuario_id($usuario_id)
    {
        $this->db->select("CONCAT(usu.nombres, ' ', usu.apellido_paterno ) as nombre_completo, ro.name, usu.active, usu.email");
        $this->db->select("ro.name as rol");
        $this->db->from('usuarios usu');
        $this->db->join(' roles ro', 'usu.role_id = ro.id', 'inner');
        $this->db->where('usu.id', $usuario_id);
        return $this->db->get()->result();
    }


}
