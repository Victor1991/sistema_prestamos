<?php
class Prestamos_model extends CI_Model {

     public function insert_prestamos($save){
        $this->db->insert('prestamos', $save);
        return $this->db->insert_id();
    }


    public function update_prestamos($save, $prestamo_id)
    {
         return $this->db->where('id', $prestamo_id)->update('prestamos', $save);
    }


    public function delete_prestamos($prestamo_id)
    {
        return $this->db->where('id', $prestamo_id)->delete('prestamos');
    }

     public function delete_for_prestamos($prestamo_id)
     {
          return $this->db->where('prestamo_id', $prestamo_id)->delete('pagos');
     }


     public function prestos_hijos($prestamos_padre){
     }


     public function inser_pagos_bach($data){
          $this->db->insert_batch('pagos', $data);
     }

     public function get_consult(){
          $this->db->select('clientes.*, prestamos.*, prestamos.id as prestamo_id')
               ->from('prestamos')
               ->join('clientes', 'clientes.id = prestamos.cliente_id', 'left');
     }

     public function params($params = array()){
          $this->db->where('prestamo_padre IS NULL', null, false);
          if (isset($params['username'])) {
              $this->db->like("clientes.nombres", $params['username']);
              $this->db->or_like("clientes.apellido_paterno", $params['username']);
              $this->db->or_like("clientes.apellido_materno", $params['username']);
         }
     }

     public function num_clientes($params = array()) {
          $this->get_consult();
          $this->params($params);
          return $this->db->count_all_results();
     }

     public function get_many($params = array(), $orden = NULL, $limit = 0, $start = 0) {
          $this->get_consult();
          $this->params($params);
          $this->db->limit($limit, $start);
          $prestamos = $this->db->get()->result();
          foreach ($prestamos as $key => $prestamo) {
               $prestamo->hijos = $this->get_prestamos_hijos($prestamo->prestamo_id);
          }
          return $prestamos;
     }

     public function get_prestamos_hijos($prestamos_id){
          return $this->db->where('prestamo_padre', $prestamos_id)->from('prestamos')->count_all_results();
     }

     public function get_by_id($id){
          return $this->db->select("*", FALSE)->from("prestamos")->where("id", $id)->get()->row();
     }

     // PAGOS
     public function pagos($prestamos_id){
          $this->db->where('prestamo_id', $prestamos_id);
          return $this->db->get('pagos')->result();
     }

     public function pagos_mes_anio($prestamos_id, $mes, $anio, $array = false){

          $this->db->select('pagos_realizados.*, cat.nombre as cat_pago');
          $this->db->from('pagos_realizados');
          $this->db->where('prestamo_id', $prestamos_id);
          $this->db->where('MONTH(fecha_pago)', $mes);
          $this->db->where('YEAR(fecha_pago)', $anio);
          $this->db->join('catalogo_tipo_pagos cat', 'cat.id = pagos_realizados.tipo_pago_id');
          if ($array) {
               return $this->db->get()->result_array();
          }else{
               return $this->db->get()->result();
          }
     }

     public function guardar_pago($save){
          $this->db->insert('pagos_realizados',$save);
          return $insert_id = $this->db->insert_id();
     }

     public function editar_pago_prestamo($id, $save)
     {
          return $this->db->where('id', $id)->update('pagos', $save);
     }

     public function prestamos_hijos($prestamo_padre){
          $this->db->where('prestamo_padre', $prestamo_padre);
          return $this->db->get('prestamos')->result_array();
     }

     public function get_prestamos_ultimo_pago($prestamo_id){
          $tabla = array();

          // Meses
          $meses = $this->meses;

          // pagos que se realizarán
          $pagos = $this->pagos($prestamo_id);
          // numero de pago
          $num = 1;
          // informacion de prestamo
          $prest = $this->get_by_id($prestamo_id);
          // Obtenemos información de interes
          $tasa_interes = 0;
          if ($prest->interes != 0 || $prest->interes != '' ) {
               $interes_decimal = $prest->interes * 0.01;
               $tasa_interes = $interes_decimal / 12;
          }
          // Monto total prestado
          $monto = $prest->cantidad_anterior + $prest->monto - $prest->enganche;
          $adeudo['monto'] = $monto;
          $adeudo['pago_id'] = '';
          foreach ($pagos as $key => $pago) {
               if ($key == 0) {
                    $adeudo['pago_id'] = $pago->id;
               }

               $int = $monto * $tasa_interes;
               $pag_mes = $monto + $int;
               $amortizacion = $prest->cuota_mensual - $int;

               $anio = date('Y', strtotime($pago->fecha_pago));
               $month = date('m', strtotime($pago->fecha_pago));
               $mot = date('n', strtotime($pago->fecha_pago));
               $pagos_realizados = $this->pagos_mes_anio($prestamo_id, $month, $anio, true);
               $sum_pagos = array_sum(array_column($pagos_realizados, 'monto'));
               $pag = $int - $sum_pagos;

               $tabla[$key]['num'] = $num;
               $tabla[$key]['renovacion'] = $pago->renovacion;
               $tabla[$key]['pago_id'] = $pago->id;
               $tabla[$key]['capital'] = number_format($monto, 2, '.', '');
               $tabla[$key]['interes'] =  number_format($int, 2, '.', '');
               $tabla[$key]['cuota'] = $prest->cuota_mensual;
               $tabla[$key]['amortizacion'] = number_format($amortizacion, 2, '.', '');
               $tabla[$key]['mes/anio'] = $meses[$mot].'-'.$anio;
               $tabla[$key]['pagos'] = $pagos_realizados;
               $tabla[$key]['adeudo'] = $pagos_realizados;

               $mon_mas_int = $monto + $int;
               $monto  =  $mon_mas_int - $sum_pagos;
               $num++;
               $tabla[$key]['sig_monto'] = $monto;

               if ($sum_pagos > 0) {
                    $adeudo['monto'] =  number_format($monto, 2, '.','');
                    $adeudo['pago_id'] = $pago->id;
               }


          }
          return array($tabla, $adeudo);
     }


}
