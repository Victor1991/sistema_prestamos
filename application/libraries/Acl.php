<?php

class Acl {

    private $_id;
    private $_rol;
    private $_permisos;

    public function __construct($id = false) {
        if ($id) {
            $this->_id = (int) $id;
        } else {
            if (get_instance()->session->userdata('usuario_id')) {
                $this->_id = get_instance()->session->userdata('usuario_id');
            } else {
                $this->_id = 0;
            }
        }

        $this->_rol = $this->get_rol_usuario();
        $this->_permisos = $this->get_permisos_rol();
        $this->init_acl();
    }

    public function set_id($id) {
        $this->_id = (int)$id;
        $this->_rol = $this->get_rol_usuario();
        $this->_permisos = $this->get_permisos_rol();
    }
    
    public function set_rol($rol) {
        $this->_rol = (int)$rol;
        $this->_id = 0;
        $this->_permisos = $this->get_permisos_rol();
    }
    
    public function init_acl() {
        $this->_permisos = array_merge(
                $this->_permisos, $this->get_permisos_usuario()
        );
    }

    public function get_usuario_id() {
        return $this->_id;
    }
    
    public function get_rol() {
        return $this->_rol;
    }

    public function get_rol_usuario() {
        $ci = &get_instance();
        $usuario = $ci->db->select('role_id')->from('usuarios')
                ->where('id', $this->_id)->get()->row();
        
        if ($usuario) {
            return $usuario->role_id;
        } else {
            return 0;
        }
    }

    public function get_permisos_role_id() {
        $ci = &get_instance();
        
        $ids = $ci->db->select('permiso_id')->from('roles_permisos')
                ->where('role_id', $this->_rol)->get()->result();
        
        $id = array();

        foreach ($ids as $value) {
            $id[] = $value->permiso_id;
        }

        return $id;
    }

    public function get_permisos_rol() {
        $ci = &get_instance();
        
        $permisos = $ci->db->select('permisos.*, roles_permisos.valor')
                ->from('roles_permisos')
                ->join('permisos', 'permisos.id = roles_permisos.permiso_id', 'inner')
                ->where('roles_permisos.rol_id', $this->_rol)->get()->result();

        $data = array();
        
        foreach ($permisos as $value) {
            $value->valor = $value->valor ? TRUE : FALSE;
            $data[$value->keyword] = $value;
        }
        
        return $data;
    }

    public function get_permisos_usuario() {
        //si un usuario puede tener permisos particulares
        $data = array();
        
        return $data;
    }

    public function get_permisos() {
        if (isset($this->_permisos) && count($this->_permisos)) {
            return $this->_permisos;
        } else {
            return false;
        }
    }

    public function permiso($key) {
        #echo "permisos: ". var_dump($this->_id); die;
        if (array_key_exists($key, $this->_permisos)) {
            if ($this->_permisos[$key]->valor == TRUE || $this->_permisos[$key]->valor == 1) {
                return TRUE;
            }
        }

        return FALSE;
    }

    public function acceso($key) {
        return $this->permiso($key);
    }

}