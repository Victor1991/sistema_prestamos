<?php

class MY_Controller extends CI_Controller {
    public $seccion;
    public $nombre_seccion;
    public $meses;


    public function __construct() {
        parent::__construct();
        $ci = get_instance();
        $this->load->model(array('Usuarios_model', 'rol_model', 'Notificaciones_model'));

        $ci->load->library('auth');

        $ci->assets['js'] = array();
        $ci->assets['css'] = array();

         if (!$this->auth->is_logged_in()) {
            redirect('login');
        }

     $this->meses = array('', 'Enero' , 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

        // Roles
        $this->usuario = $this->Usuarios_model->get_by_id($this->session->userdata('usuario_id'));
        $this->usuario->rol = $this->rol_model->get_by_id($this->usuario->id);
        $this->load->library('acl');
    }

    public function add_asset($type, $var) {
        array_push(get_instance()->assets[$type], $var);
    }

    function view($view, $vars = array(), $string = false) {
        if ($string) {
            $result = $this->load->view('header', $vars, true);
            $result .= $this->load->view($view, $vars, true);
            $result .= $this->load->view('footer', $vars, true);

            return $result;
        } else {
            $this->load->view('header', $vars);
            $this->load->view($view, $vars);
            $this->load->view('footer', $vars);
        }
    }

}
