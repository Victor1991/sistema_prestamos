<?php

defined('BASEPATH') OR exit('No direct script access allowed.');


if (!function_exists('create_pagination')) {

    /**
     * The Pagination helper cuts out some of the bumf of normal pagination
     *
     * @param string $uri The current URI.
     * @param int $total_rows The total of the items to paginate.
     * @param int|null $limit How many to show at a time.
     * @param int $uri_segment The current page.
     * @param boolean $full_tag_wrap Option for the Pagination::create_links()
     * @return array The pagination array. 
     * @see Pagination::create_links()
     */
    function create_pagination($uri, $total_rows, $limit = 10, $uri_segment = 4, $full_tag_wrap = true , $string_query = false, $page_query = false) {
        $ci = & get_instance();
        $ci->load->library('pagination');

        $current_page = $ci->uri->segment($uri_segment, 0);
        $suffix = $ci->config->item('url_suffix');

        // Initialize pagination
        $ci->pagination->initialize(array(
            //info 
            'reuse_query_string' => $string_query,
            'page_query_string' => $page_query,

            'suffix' => $suffix,
            'base_url' => (!$suffix) ? rtrim(site_url($uri), $suffix) : site_url($uri),
            'total_rows' => $total_rows,
            'per_page' => $limit,
            'uri_segment' => $uri_segment,
            // 'full_tag_open' => '<ul class="pagination">',
            // 'full_tag_close' => '</ul>',
            // 'first_link' => 'Primero',
            // 'first_tag_open' => '<li class="prev page">',
            // 'first_tag_close' => '</li>',
            // 'last_link' => 'Último &raquo;',
            // 'last_tag_open' => '<li class="next page">',
            // 'last_tag_close' => '</li>',
            // 'next_link' => '<i class="fa fa-chevron-right"></i>',
            // 'next_tag_open' => '<li class="next page">',
            // 'next_tag_close' => '</li>',
            // 'prev_link' => '<i class="fa fa-chevron-left"></i>',
            // 'prev_tag_open' => '<li class="prev page">',
            // 'prev_tag_close' => '</li>',
            // 'cur_tag_open' => '<li class="active"><a href="">',
            // 'cur_tag_close' => '</a></li>',
            // 'num_tag_open' => '<li class="page">',
            // 'num_tag_close' => '</li>',
             'first_link' => 'Primero',
              'first_tag_open' => '<li>',
              'first_tag_close' => '</li>',
              'last_link' => 'Último',
              'last_tag_open' => '<li>',
              'last_tag_close' => '</li>',
              'full_tag_open' => '<nav><u, class="pagination nobottommargin">',
              'full_tag_close' => '</ul></na,>',
              'cur_tag_open' => '<li class="active"><a href="#">',
              'cur_tag_close' => '</a></l,>',
              'num_tag_open' => '<li>',
              'num_tag_close' => '</li>',
              'prev_link' => '&laquo;',
              'prev_tag_open' => '<li>',
              'prev_tag_close' => '</li>',
              'next_link' => '&raquo;',
              'next_tag_open' => '<li>',
              'next_tag_close' => '</li>',
        ));

        //$offset = $limit * ($current_page - 1);
        $offset = $current_page;

        //avoid having a negative offset
        if ($offset < 0)
            $offset = 0;

        return array(
            'current_page' => $current_page,
            'per_page' => $limit,
            'limit' => $limit,
            'offset' => $offset,
            'links' => $ci->pagination->create_links()
        );
    }

}