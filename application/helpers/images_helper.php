<?php

function upload_images($files, $name) {

    $path = FCPATH . 'uploads/gastos/';
    $nombres = array();
    $CI = & get_instance();
    $CI->load->helper('dump');
    foreach ($files[$name]['name'] as $value) {
        if ($value != '') {
            $pos = strrpos($value, '.', -1);
            $type = substr($value, $pos, strlen($value) - 1);
            $nombres[] = sha1(time() . uniqid()) . $type;
        }else{
            $nombres[] = '';
        }
    }
    foreach ($files[$name]['tmp_name'] as $key => $value) {
        if ($value != '') {
            move_uploaded_file($value, $path . $nombres[$key]);
        }
    }
    return $nombres;
}
