<?php

function show_alerts() {
    $ci = & get_instance();
    $errores = $ci->session->flashdata('errores');
    $mensajes = $ci->session->flashdata('mensajes');
    $notificaciones = $ci->session->flashdata('notificaciones');
    $alertas = $ci->session->flashdata('alertas');
    
    if ($errores) {
        alert_error($errores);
    }
    if ($mensajes) {
        alert_success($mensajes);
    }
    if ($notificaciones) {
        alert_info($notificaciones);
    }
    if ($alertas) {
        alert_warning($alertas);
    }
    
}

function alert_success($message) {
    alert_message($message, 'success');
}

function alert_error($message) {
    alert_message($message, 'danger');
}

function alert_warning($mesage) {
    alert_message($message, 'warning');
}

function alert_info($message) {
    alert_message($message, 'info');
}

function alert_message($message, $type) {
    echo '<div class="alert alert-' . $type . ' alert-dismissible" role="alert">'
        . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
        . $message . '</div>';
}