<?php

function theme_assets($uri) {
    $CI = & get_instance();
    return $CI->config->base_url('application/themes/' . FRONT_THEME . '/assets/' . $uri);
}

function admin_assets($uri) { 
    $CI = & get_instance();
    return $CI->config->base_url('assets/' . $uri);
} 