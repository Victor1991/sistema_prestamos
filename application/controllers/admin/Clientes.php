<?php
class Clientes extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Clientes_model');
		$session = $this->auth->is_logged_in();
		if ($session == FALSE) {
			redirect('login');
		}

		$permiso = $this->acl->get_permisos();
		if(!$permiso['listar_usu']){
			redirect('/admin');
		}
		$this->seccion = '3';
		$this->nombre_seccion = 'Clientes';
		$this->load->model(['clientes_model', 'prestamos_model']);



	}

	public function index() {
		$this->load->helper('form');
		$data['username'] = "";
		$data['role_id'] = "";
		$data['errors'] = $this->session->flashdata('errors');
		$data['messages'] = $this->session->flashdata('messages');

		$this->add_asset('js', 'js/plugins/jquery-bootgrid/jquery.bootgrid.js');
		$this->add_asset('js', 'js/plugins/jquery-bootgrid/jquery.bootgrid.fa.js');
		$this->add_asset('css', 'js/plugins/jquery-bootgrid/jquery.bootgrid.css');

		$this->add_asset('js', 'js/plugins/posts/admin-posts.js');
		$this->add_asset('js', 'js/plugins/sweetalert/sweetalert.min.js');
		$this->add_asset('css', 'css/plugins/sweetalert/sweetalert.css');
		$this->add_asset('js', 'js/lista_clientes.js');
		$this->view('admin/clientes/index', $data);
	}

	public function table_clientes()
	{
		$this->load->library('pagination');
		$pro_page = 10;
		$offset=(int)$this->input->get('per_page');
		$offset_enviar = $pro_page * $offset;

		$where = array('active' => 0);
		$orden = array('columna' => 'nombres', 'orden' => 'desc');

		if ($this->input->get('orden')) {
			$orden['columna'] = $this->input->get('columna');
			$orden['orden'] = $this->input->get('orden');
		}

		// Buscador por nombre
		if ($this->input->get('username')) {
			$where['username'] = $this->input->get('username');
			$data['username'] = $this->input->get('username');
		}

		//$config['base_url'] = site_url('admin/usuarios/');
		//$config['reuse_query_string'] = true;
		$config['page_query_string'] = TRUE;
		$config['base_url'] ='table_usuarios';
		$config['total_rows'] = $this->Clientes_model->num_clientes($where);
		$config['per_page'] = $pro_page;
		$config['num_links'] = 5;
		$config['uri_segment'] = 3;
		$config['first_link'] = 'Primero';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['attributes'] = array('class' => 'page-link');
		$config['last_link'] = 'Último';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';
		$config['full_tag_open'] = '<nav><ul class="pagination">';
		$config['full_tag_close'] = '</ul></nav>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';

		$this->pagination->initialize($config);
		$page_link = $this->pagination->create_links();

		$list = $this->Clientes_model->get_many($where, $orden, $pro_page, $offset);

		// _dump($list);

		$table = '';
		foreach($list as $row):
			$table.='<tr>';
			$table.='<td>'.$row->nombres.' '.$row->apellido_paterno.''.$row->apellido_materno.'</td>';
			$table.='<td>'.$row->telefono	.'</td>';
			$table.='<td>'.$row->celular.'</td>';

			$span = '';
			if ($row->cliente) {
				$span .= '<span class="badge badge-info">Cliente</span>';
			}

			if ($row->inversinista) {
				$span .= '<span class="m-l-10 badge badge-primary">Inversinista</span>';

			}

			$table.='<td><h5>'.$span.'</h5></td>';

			//filto tipo de estatus

			// ver botones

			$table.='<td class="center"><a href="clientes/form/'.$row->id.'" class="btn btn-info btn-sm " title="Editar"><i class="fa fa-edit"></i></a> <a href="#"  class="btn btn-danger btn-sm btn-delete" title="Eliminar" onclick="eliminar('.$row->id.')"><i class="fa fa-times"></i></a></td>';

			$table.='</tr>';
		endforeach;

		$data =array('table'=>$table, 'page'=>$page_link );

		$this->output->set_content_type('application/json');
		echo json_encode($data);

	}


	public function form($cliente_id = FALSE) {

		$this->load->library('form_validation');
		$this->load->helper('form');

		//variable para textos
		$data['titulo_form'] = 'Nuevo Cliente';
		$data['errors'] = $this->session->flashdata('errors');
		$data['messages'] = $this->session->flashdata('messages');

		//variable para datos
		$data['email'] = "";
		$data['nombres'] = "";
		$data['apellido_paterno'] = "";
		$data['apellido_materno'] = "";
		$data['telefono'] = "";
		$data['celular'] = "";
		$data['ine'] = '';
		$data['comprobante_domicilio'] = "";
		$data['cliente'] = "";
		$data['inversinista'] = "";

		if ($cliente_id) {
			$data['titulo_form'] = 'Editar Cliente';
			$data  = array_merge($data, (array)$this->Clientes_model->get_by_id($cliente_id));
		}

		$this->form_validation->set_rules('nombres', 'Nombre', 'required');
		if (!isset($_POST['cliente']) && !isset($_POST['inversinista'])) {
			$this->form_validation->set_rules('cliente', 'Tipo de cliente', 'required');
		}
		$this->form_validation->set_message('required', '%s es requerido');

		if ($this->form_validation->run() === FALSE) {

			$data['errors'] = validation_errors();
			$this->view('admin/clientes/form', $data);

		}else{

			$save['nombres'] = strip_tags($this->input->post('nombres'));
			$save['apellido_paterno'] = strip_tags($this->input->post('apellido_paterno'));
			$save['apellido_materno'] = strip_tags($this->input->post('apellido_materno'));
			$save['telefono'] = strip_tags($this->input->post('telefono'));
			$save['celular'] = strip_tags($this->input->post('celular'));
			$save['direccion'] = strip_tags($this->input->post('direccion'));
			$save['cliente'] = strip_tags($this->input->post('cliente'));
			$save['inversinista'] = strip_tags($this->input->post('inversinista'));
			// $save['ine'] = strip_tags($this->input->post('ine'));
			// $save['comprobante_domicilio'] = (int)$this->input->post('comprobante_domicilio');

			// Image
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = 2000;
			$config['max_width'] = 1500;
			$config['max_height'] = 1500;
			$config['encrypt_name'] = TRUE;
			$config['upload_path'] = './assets/files';
			if ($_FILES['ine']['name']) {
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('ine')) {
					$this->session->set_flashdata('errors',$this->upload->display_errors());
				} else {
					$data = array('image_metadata' => $this->upload->data());
					 $save['ine'] = $this->upload->data('file_name');
				}
			}

			if ($_FILES['comprobante_domicilio']['name']) {
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('comprobante_domicilio')) {
					$this->session->set_flashdata('errors',$this->upload->display_errors());
				} else {
					 $save['comprobante_domicilio'] = $this->upload->data('file_name');
				}
			}

			if ($cliente_id) {
				$save['modified_date'] = date("Y-m-d");
				$data_update = $this->Clientes_model->update($save, $cliente_id);
				if ($data_update) {
					$this->session->set_flashdata('messages', 'Los datos han sido actualizados');
				}else{
					$this->session->set_flashdata('errors', 'Los datos no han podido ser guardados');
				}
				redirect('/admin/clientes');
			}else{
				$save['created_date'] = date("Y-m-d");
				$data_save = $this->Clientes_model->insert($save);
				if ($data_save) {
					$this->session->set_flashdata('messages', 'Los datos han sido guardados');
				} else {
					$this->session->set_flashdata('errors', 'Los datos no han podido ser guardados');
				}
				redirect('/admin/clientes');
			}

		}

	}

	public function eliminar_cliente($cliente_id = FALSE)
	{
		$eliminado = $this->Clientes_model->delete($cliente_id);
		if ($eliminado) {
			$this->session->set_flashdata('messages', 'Exito al eliminar al usuario');
		}else{
			$this->session->set_flashdata('errors', 'Error al eliminar al usuario');
		}
		redirect('/admin/clientes');

	}

}
