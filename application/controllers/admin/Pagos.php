<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Pagos extends MY_Controller{
     public function __construct(){
          parent::__construct();
          $session = $this->auth->is_logged_in();
          if ($session == FALSE) {
               redirect('login');
          }
          $this->load->helper(array('form', 'url'));
          $this->seccion = '5';
          $this->load->model(['prestamos_model','clientes_model', 'catalogos_model']);
          $this->nombre_seccion = 'Pagos';
     }

     public function index(){
          $this->load->helper('form');
          $data['username'] = "";
          $data['errors'] = $this->session->flashdata('errors');
          $data['messages'] = $this->session->flashdata('messages');

          $this->add_asset('js', 'js/plugins/jquery-bootgrid/jquery.bootgrid.js');
          $this->add_asset('js', 'js/plugins/jquery-bootgrid/jquery.bootgrid.fa.js');
          $this->add_asset('css', 'js/plugins/jquery-bootgrid/jquery.bootgrid.css');

          $this->add_asset('js', 'js/plugins/posts/admin-posts.js');
          $this->add_asset('js', 'js/plugins/sweetalert/sweetalert.min.js');
          $this->add_asset('css', 'css/plugins/sweetalert/sweetalert.css');
          $this->add_asset('js', 'js/lista_prestamos.js');

          $this->view('admin/prestamos/lista', $data);
     }

     public function form($prestamo_id = false){
          $this->add_asset('js', 'js/plugins/sweetalert/sweetalert.min.js');
          $this->add_asset('css', 'css/plugins/sweetalert/sweetalert.css');
          $this->add_asset('js', 'js/form_pagos.js');

          $data = array(
               'titulo_form' => 'Cotizar prestamo',
               'errors' => $this->session->flashdata('errors'),
               'messages' => $this->session->flashdata('messages'),
               'monto' => 0,
               'enganche' => 0,
               'interes' => 17,
               'meses' => 12,
               'cliente_id' => '',
               'creado' => '',
               'id' => ''
          );

          $data['clientes'] = $this->clientes_model->get_all();
          $data['cat_pagos'] = $this->catalogos_model->get_tipo_pago();

          $data['meses_str'] = $this->meses;
          $prestamos_hijo = $this->prestamos_model->prestamos_hijos($prestamo_id);
          if ($prestamo_id) {
               if (count($prestamos_hijo) > 1) {
                    $data['prestamo'] = array_merge($data, (array)$this->prestamos_model->get_by_id($prestamo_id));
                    // Prestamo padre
                    $tbl_pagos = $this->prestamos_model->get_prestamos_ultimo_pago($prestamo_id);
                    $data['tabla_pagos'] = $tbl_pagos[0];
                    $data['adeudo'] = $tbl_pagos[1]['monto'];
                    $data['ultimo_pago_id'] = $tbl_pagos[1]['pago_id'];
                    $data['id'] = $prestamo_id;

                    // Prestmos prestos_hijos
                    $data['prestamos_hijo'] = array();
                    foreach ($prestamos_hijo as $key => $prest) {
                         $data['prestamos_hijo'][$key]['prestamo'] = $prest;
                         $tbl_pagos = $this->prestamos_model->get_prestamos_ultimo_pago($prest['id']);
                         $data['prestamos_hijo'][$key]['pagos'] = $tbl_pagos[0];
                         $data['adeudo'] = $tbl_pagos[1]['monto'];
                         $data['ultimo_pago_id'] = $tbl_pagos[1]['pago_id'];
                         $data['id'] = $prest['id'];
                    }

               }else{
                    $data = array_merge($data, (array)$this->prestamos_model->get_by_id($prestamo_id));

               }
          }

          if (count($prestamos_hijo) > 1) {
               $this->view('admin/pagos/form_renovacion', $data);

          }else{
               $this->view('admin/pagos/form', $data);
          }

     }

     public function pagos_prestamos(){
          $prestamo_id = $this->input->post('prestamo_id');
          $mes = $this->input->post('mes');
          $anio = $this->input->post('anio');
          $pagos['pago'] = $this->prestamos_model->pagos_mes_anio($prestamo_id, $mes, $anio);
          $pagos['fecha'] = date("Y-m-d", strtotime($anio.'-'.$mes.'-02')); ;
          echo json_encode($pagos);
     }

     public function guardar_pago(){
          $save['fecha_pago'] = $this->input->post('fecha_pago');
          $save['prestamo_id'] = $this->input->post('prestamo_id');
          $save['tipo_pago_id'] = $this->input->post('tipo_pago_id');
          $save['monto'] = $this->input->post('monto');
          $mensaje =  array('type' => 'error', 'mensaje' => 'Ups hubo un error inténtalo más tarde ' );

          $save['imagen'] = '';
          if ($_FILES['foto1']['name'] != '') {
               $config['upload_path'] = './uploads/';
               $config['allowed_types'] = 'gif|jpg|png';
               $config['max_size'] = '10000';
               $config['remove_spaces'] = TRUE;
                $config['encrypt_name'] = TRUE;

               $this->load->library('upload', $config);
               if($this->upload->do_upload('foto1')){
                    $save['imagen'] = $this->upload->data('file_name');
               } else {
                    $mensaje['mensaje'] = $this->upload->display_errors();
                    echo json_encode($mensaje);
               }
          }

          $pago = $this->prestamos_model->guardar_pago($save);
          if ($pago) {
               $mensaje['mensaje'] = 'Pago registrado correctamente';
               $mensaje['type'] = 'exito';
          }else{
               $mensaje['mensaje'] = 'Error al registrar el presatamo';
          }

          echo json_encode($mensaje);
     }

     public function tabla_principal(){
          $this->load->library('pagination');
          $pro_page = 10;
          $offset = (int)$this->input->get('per_page');
          $offset_enviar = $pro_page * $offset;
          $where = array('active' => 0);
          $orden = array('columna' => 'nombres', 'orden' => 'desc');
          if ($this->input->get('orden')) {
               $orden['columna'] = $this->input->get('columna');
               $orden['orden'] = $this->input->get('orden');
          }
          // Buscador por nombre
          if ($this->input->get('username')) {
               $where['username'] = $this->input->get('username');
               $data['username'] = $this->input->get('username');
          }
          //$config['base_url'] = site_url('admin/usuarios/');
          //$config['reuse_query_string'] = true;
          $config['page_query_string'] = TRUE;
          $config['base_url'] = 'table_usuarios';
          $config['total_rows'] = $this->prestamos_model->num_clientes($where);
          $config['per_page'] = $pro_page;
          $config['num_links'] = 5;
		$config['uri_segment'] = 3;
		$config['first_link'] = 'Primero';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['attributes'] = array('class' => 'page-link');
		$config['last_link'] = 'Último';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';
		$config['full_tag_open'] = '<nav><ul class="pagination">';
		$config['full_tag_close'] = '</ul></nav>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';

          $this->pagination->initialize($config);
          $page_link = $this->pagination->create_links();
          $list = $this->prestamos_model->get_many($where, $orden, $pro_page, $offset);
          // _dump($list);
          $table = '';
          foreach($list as $row):
               $table .= '<tr>';
               $table .= '<td>'.$row->id.'</td>';
               $table .= '<td>'.$row->nombres.
               ' '.$row->apellido_paterno.
               ''.$row->apellido_materno.
               '</td>';
               $table .= '<td>$ '. number_format($row->monto, 2, '.', ',').
               '</td>';
               $table .= '<td>'.$row->meses.
               '</td>';
               $table .= '<td>'.$row->creado.
               '</td>';
               //filto tipo de estatus
               // ver botones
               $table .= '<td class="center"><a href="pagos/form/'.$row->id.'" class="btn btn-info btn-sm " title="Pagos"><i class="fas fa-money-check-alt"></i></a></td>';
               $table  .= '</tr>';
          endforeach;
          $data = array('table' => $table, 'page' => $page_link);
          $this->output->set_content_type('application/json');
          echo json_encode($data);
     }
}


?>
