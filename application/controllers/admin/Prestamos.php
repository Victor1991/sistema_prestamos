<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prestamos extends MY_Controller {

     public function __construct() {
          parent::__construct();
          $session = $this->auth->is_logged_in();
          if ($session == FALSE) {
               redirect('login');
          }

          $this->meses = array('', 'Enero' , 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

          $this->seccion = '4';
          $this->nombre_seccion = 'Prestamos';
          $this->load->model(['clientes_model', 'prestamos_model', 'catalogos_model']);
     }



     public function index() {
          $this->load->helper('form');
          $data['username'] = "";
          $data['errors'] = $this->session->flashdata('errors');
          $data['messages'] = $this->session->flashdata('messages');

          $this->add_asset('js', 'js/plugins/jquery-bootgrid/jquery.bootgrid.js');
          $this->add_asset('js', 'js/plugins/jquery-bootgrid/jquery.bootgrid.fa.js');
          $this->add_asset('css', 'js/plugins/jquery-bootgrid/jquery.bootgrid.css');

          $this->add_asset('js', 'js/plugins/posts/admin-posts.js');
          $this->add_asset('js', 'js/plugins/sweetalert/sweetalert.min.js');
          $this->add_asset('css', 'css/plugins/sweetalert/sweetalert.css');
          $this->add_asset('js', 'js/lista_prestamos.js');

          $this->view('admin/prestamos/lista', $data);
     }

     public function form($pretamos_id = false){
          $this->add_asset('js', 'js/form_prestamos.js');

          $data = array(
               'titulo_form' => 'Cotizar prestamo',
               'errors' => $this->session->flashdata('errors'),
               'messages' => $this->session->flashdata('messages'),
               'monto' => 0,
               'enganche' => 0,
               'interes' => 17,
               'meses' => 12,
               'fecha_inicio' => date('Y-m-d'),
               'cuota_mensual' => 0,
               'cliente_id' => '',
               'id' => ''
          );
          if ($pretamos_id) {
               $data = array_merge($data, (array)$this->prestamos_model->get_by_id($pretamos_id));
          }

          $data['clientes'] = $this->clientes_model->get_all();
          $this->view('admin/prestamos/form', $data);
     }

     public function prestamos_data(){
          $tabla = json_decode($this->input->post('tabla'));
          $intereses = json_decode($this->input->post('intereses'));
          $monto = $this->input->post('monto');
          $enganche = $this->input->post('enganche');
          $interes = $this->input->post('interes');
          $meses = $this->input->post('meses');

          $mensaje =  array('type' => 'error', 'mensaje' => 'Ups hubo un error inténtalo más tarde ' );

          $id = $this->input->post('id');
          if ($this->input->post('prestamo_id')) {
               $prestamo['prestamo_padre']  =  $this->input->post('prestamo_id');
          }
          if ($this->input->post('cantidad_anterior')) {
               $prestamo['cantidad_anterior']  =  $this->input->post('cantidad_anterior');
          }
          $prestamo['cliente_id']  =  $this->input->post('cliente_id');
          $prestamo['monto']  =  $this->input->post('monto');
          $prestamo['enganche']  = $this->input->post('enganche');
          $prestamo['interes']  = $this->input->post('interes');
          $prestamo['meses']  = $this->input->post('meses');

          $prestamo['tasa_interes']  = $intereses->tasa_interes;
          $prestamo['interes_general']  = $intereses->interes_general;
          $prestamo['tasa_anual']  = $intereses->tasa_anual;
          $prestamo['fecha_inicio']  = $this->input->post('fecha_inicio');
          $prestamo['cuota_mensual']  = $intereses->cuota_mensual;
          $prestamo['creado'] = date('Y-m-d');


          if ($id) {
               $prestamo_id = $id;
               $this->prestamos_model->delete_for_prestamos($prestamo_id);
               $this->prestamos_model->update_prestamos($prestamo, $id);
          }else{
               $prestamo_id = $this->prestamos_model->insert_prestamos($prestamo);
          }

          if ($this->input->post('ultimo_pago_id')) {
               $this->prestamos_model->editar_pago_prestamo($this->input->post('ultimo_pago_id'), array('renovacion' => 1));
          }


          if ($prestamo) {
               foreach ($tabla as $key => $pago) {
                    $pago->prestamo_id = $prestamo_id;
               }
               $prestamo = $this->prestamos_model->inser_pagos_bach((array)$tabla);
               if ($id) {
                    $mensaje['mensaje'] = 'Prestamo editado correctamente';
               }else{
                    $mensaje['mensaje'] = 'Prestamo creado correctamente';
               }
               $mensaje['type'] = 'exito';
          }else{
               $mensaje['mensaje'] = 'Error al crear el presatamo';
          }

          echo json_encode($mensaje);
     }

     public function renovar_prestamo($prestamo_id = false){

          $this->nombre_seccion = 'Renovar prestamos';

          $this->add_asset('js', 'js/renovar_prestamos.js');

          if ($prestamo_id) {
               $data = array(
                    'titulo_form' => 'Cotizar prestamo',
                    'errors' => $this->session->flashdata('errors'),
                    'messages' => $this->session->flashdata('messages'),
                    'monto' => 0,
                    'enganche' => 0,
                    'interes' => 17,
                    'meses' => 12,
                    'cliente_id' => '',
                    'creado' => '',
                    'id' => '',
                    'ultimo_pago_id' => ''
               );
               $data['prestamo'] = array_merge($data, (array)$this->prestamos_model->get_by_id($prestamo_id));
               // Prestamo padre
               $tbl_pagos = $this->prestamos_model->get_prestamos_ultimo_pago($prestamo_id);
               $data['tabla_pagos'] = $tbl_pagos[0];
               $data['adeudo'] = $tbl_pagos[1]['monto'];
               $data['ultimo_pago_id'] = $tbl_pagos[1]['pago_id'];

               // Prestmos prestos_hijos
               $prestamos_hijo = $this->prestamos_model->prestamos_hijos($prestamo_id);
               $data['prestamos_hijo'] = array();
               foreach ($prestamos_hijo as $key => $prest) {
                    $data['prestamos_hijo'][$key]['prestamo'] = $prest;
                    $this->prestamos_model->get_prestamos_ultimo_pago($prest['id']);
                    $tbl_pagos = $this->prestamos_model->get_prestamos_ultimo_pago($prest['id']);
                    $data['prestamos_hijo'][$key]['pagos'] = $tbl_pagos[0];
                    $data['adeudo'] = $tbl_pagos[1]['monto'];
                    $data['ultimo_pago_id'] = $tbl_pagos[1]['pago_id'];
               }

     

               $data['clientes'] = $this->clientes_model->get_all();
               $data['cat_pagos'] = $this->catalogos_model->get_tipo_pago();
               $data['meses_str'] = $this->meses;

               $interes_decimal = $data['interes'] * 0.01;
               $tasa_interes = $interes_decimal / 12;
               $int = $data['monto'] * $tasa_interes;


               $this->view('admin/renovacion/index', $data);
          }else{
               show_404();
          }
     }


     public function tabla_principal(){
          $this->load->library('pagination');
          $pro_page = 10;
          $offset = (int)$this->input->get('per_page');
          $offset_enviar = $pro_page * $offset;
          $where = array('active' => 0);
          $orden = array('columna' => 'nombres', 'orden' => 'desc');
          if ($this->input->get('orden')) {
               $orden['columna'] = $this->input->get('columna');
               $orden['orden'] = $this->input->get('orden');
          }
          // Buscador por nombre
          if ($this->input->get('username')) {
               $where['username'] = $this->input->get('username');
               $data['username'] = $this->input->get('username');
          }
          //$config['base_url'] = site_url('admin/usuarios/');
          //$config['reuse_query_string'] = true;
          $config['page_query_string'] = TRUE;
          $config['base_url'] = 'table_usuarios';
          $config['total_rows'] = $this->prestamos_model->num_clientes($where);
          $config['per_page'] = $pro_page;
          $config['num_links'] = 5;
          $config['uri_segment'] = 3;
          $config['first_link'] = 'Primero';
          $config['first_tag_open'] = '<li>';
          $config['first_tag_close'] = '</li>';
          $config['last_link'] = 'Último';
          $config['last_tag_open'] = '<li>';
          $config['last_tag_close'] = '</li>';
          $config['full_tag_open'] = '<nav><ul class="pagination nobottommargin">';
          $config['full_tag_close'] = '</ul></nav>';
          $config['cur_tag_open'] = '<li class="active"><a href="#">';
          $config['cur_tag_close'] = '</a></li>';
          $config['num_tag_open'] = '<li>';
          $config['num_tag_close'] = '</li>';
          $config['prev_link'] = '&laquo;';
          $config['prev_tag_open'] = '<li>';
          $config['prev_tag_close'] = '</li>';
          $config['next_link'] = '&raquo;';
          $config['next_tag_open'] = '<li>';
          $config['next_tag_close'] = '</li>';
          $this->pagination->initialize($config);
          $page_link = $this->pagination->create_links();
          $list = $this->prestamos_model->get_many($where, $orden, $pro_page, $offset);
          // _dump($list);
          $table = '';
          // _dump($list);
          foreach($list as $row):
               $table .= '<tr>';
               $table .= '<td>'.$row->id.'</td>';
               $table .= '<td>'.$row->nombres.
               ' '.$row->apellido_paterno.
               ''.$row->apellido_materno.
               '</td>';
               $table .= '<td>$ '. number_format($row->monto, 2, '.', ',').
               '</td>';
               $table .= '<td>'.$row->meses.
               '</td>';
               $table .= '<td>'.$row->creado.
               '</td>';
               //filto tipo de estatus
               $btn_hijo = '';
               if ((int)$row->hijos > 0) {
                    $btn_hijo = '<a href="prestamos/renovar_prestamo/'.$row->id.'" class="btn btn-info">
                                   <i class="fas fa-undo"></i> <span class="badge badge-light">'.$row->hijos.'</span>
                                 </a>';
               }else{
                    $btn_hijo = '<a href="prestamos/renovar_prestamo/'.$row->id.'" class="btn btn-info btn-sm " title="Editar">
                                   <i class="fa fa-eye"></i>
                                 </a>';
               }
               // ver botones
               $table .= '<td class="center">
                              '.$btn_hijo.'
                         </td>';
               $table  .= '</tr>';
          endforeach;
          $data = array('table' => $table, 'page' => $page_link);
          $this->output->set_content_type('application/json');
          echo json_encode($data);
     }



}
