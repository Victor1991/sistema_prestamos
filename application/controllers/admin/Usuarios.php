<?php class Usuarios extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Usuarios_model');
		$session = $this->auth->is_logged_in();
		if ($session == FALSE) {
			redirect('login');
		}

		$permiso = $this->acl->get_permisos();
		if(!$permiso['listar_usu']){
			redirect('/admin');
		}
		$this->seccion = '2';
		$this->nombre_seccion = 'Usuarios';


	}

	public function index() {
		$this->load->helper('form');
		$data['username'] = "";
		$data['role_id'] = "";
		$data['errors'] = $this->session->flashdata('errors');
		$data['messages'] = $this->session->flashdata('messages');

		$roles = $this->Usuarios_model->get_roles();
		$data['roles'][] = "Selecciona un Rol";
		foreach ($roles as $value) {
			$data['roles'][$value->id] = $value->name;
		}

		$this->add_asset('js', 'js/plugins/jquery-bootgrid/jquery.bootgrid.js');
		$this->add_asset('js', 'js/plugins/jquery-bootgrid/jquery.bootgrid.fa.js');
		$this->add_asset('css', 'js/plugins/jquery-bootgrid/jquery.bootgrid.css');

		$this->add_asset('js', 'js/plugins/posts/admin-posts.js');
		$this->add_asset('js', 'js/plugins/sweetalert/sweetalert.min.js');
		$this->add_asset('css', 'css/plugins/sweetalert/sweetalert.css');
		$this->add_asset('js', 'js/lista_usuario.js');
		$this->view('admin/usuarios/index', $data);
	}

	public function table_usuarios()
	{
		$this->load->library('pagination');
		$pro_page = 10;
		$offset=(int)$this->input->get('per_page');
		$offset_enviar = $pro_page * $offset;

		$where = array('active' => 0);
		$orden = array('columna' => 'username', 'orden' => 'desc');

		if ($this->input->get('role_id')) {
			$where['role_id'] = (int) $this->input->get('role_id');
			$data['role_id'] = $where['role_id'];
		}

		if ($this->input->get('orden')) {
			$orden['columna'] = $this->input->get('columna');
			$orden['orden'] = $this->input->get('orden');
		}

		// Buscador por nombre
		if ($this->input->get('username')) {
			$where['username'] = $this->input->get('username');
			$data['username'] = $this->input->get('username');
		}

		//$config['base_url'] = site_url('admin/usuarios/');
		//$config['reuse_query_string'] = true;
		$config['page_query_string'] = TRUE;
		$config['base_url'] ='table_usuarios';
		$config['total_rows'] =  $this->Usuarios_model->num_usuarios($where);
		$config['per_page'] = $pro_page;
		$config['num_links'] = 5;
		$config['uri_segment'] = 3;
		$config['first_link'] = 'Primero';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['attributes'] = array('class' => 'page-link');
		$config['last_link'] = 'Último';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';
		$config['full_tag_open'] = '<nav><ul class="pagination">';
		$config['full_tag_close'] = '</ul></nav>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';

		$this->pagination->initialize($config);
		$page_link = $this->pagination->create_links();

		$list = $this->Usuarios_model->get_many($where, $orden, $pro_page, $offset);

		$table = '';
		foreach($list as $row):
			$table.='<tr>';
			$table.='<td>'.$row->nombre_completo.'</td>';
			$table.='<td>'.$row->rol.'</td>';
			$table.='<td>'.$row->email.'</td>';

			//filto tipo de estatus
			$estatus = "";
			if ($row->active == 1) { $estatus = '<span class="label label-success">ON</span>'; } else { $estatus = '<span class="label label-danger">OFF</span>'; };
			$table.='<td>'. $estatus .'</td>';

			// ver botones
			if($row->role_id >= $_SESSION['rol_id']){
				if ($_SESSION['usuario_id'] == $row->id) {
					$table.='<td class="center"><a href="usuarios/form/'.$row->id.'" class="btn btn-info btn-sm " title="Editar"><i class="fa fa-edit"></i></a> <button type="button" class="btn btn-primary btn-sm informacion" data-toggle="modal" data-target="#myModal" id="info'.$row->id.'" ><i class="fa fa-user" aria-hidden="true"></i></button></td>';	# code...
				}else{
					$table.='<td class="center"><a href="usuarios/form/'.$row->id.'" class="btn btn-info btn-sm " title="Editar"><i class="fa fa-edit"></i></a> <a href="#" id="usu_'.$row->id.'" class="btn btn-danger btn-sm btn-delete" title="Eliminar" onclick="Eliminar(this)"><i class="fa fa-times"></i></a></td>';
				}
			}else{
				$table.='<td class="center"><button type="button" class="btn btn-primary btn-sm informacion" data-toggle="modal" data-target="#myModal" id="info'.$row->id.'" ><i class="fa fa-user" aria-hidden="true"></i></button></td>';
			}
			$table.='</tr>';
		endforeach;

		$data =array('table'=>$table, 'page'=>$page_link );

		$this->output->set_content_type('application/json');
		echo json_encode($data);

	}


	public function form($usuario_id = FALSE) {

		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->library('bcrypt');

		//assets
		$this->add_asset('js', 'js/plugins/sweetalert2-master/sweetalert2.min.js');
		$this->add_asset('css', 'js/plugins/sweetalert2-master/sweetalert2.css');
		$this->add_asset('css', 'css/witch.css');

		//variable para textos
		$data['titulo_form'] = 'Nuevo Usuario';
		$data['errors'] = $this->session->flashdata('errors');
		$data['messages'] = $this->session->flashdata('messages');
		$data['txt_contrasena'] = "Nueva Contaseña";
		$data['txt_confirmar'] = "Confirmar contraseña";

		//variable para datos
		$data['username'] = "";
		$data['role_id'] = "";
		$data['email'] = "";
		$data['nombres'] = "";
		$data['apellido_paterno'] = "";
		$data['apellido_materno'] = "";
		$data['email'] = "";
		$data['active'] = 1;
		$data['password'] = "";
		$data['confirmar_Password'] = "";

		//variable para validacion de contraseña
		$password = FALSE;
		$pass_confirm = FALSE;

		if ($usuario_id) {
			$data  = array_merge($data, (array) $this->Usuarios_model->get_usuario_by_id($usuario_id));

			if ($data['role_id'] < $_SESSION['rol_id']) {
				redirect('/admin/usuarios');
			}

			$data['vista'] = 2;
			$data['titulo_form'] = 'Editar Usuario';
			$pass = $this->input->post('password');
			$pass_confirm = $this->input->post('confirmar_password');
			if ($pass || $password) {
				$this->form_validation->set_rules('password', 'Contraseña', 'required');
				$this->form_validation->set_rules('confirmar_password', 'Confirmar contraseña', 'required|matches[password]');
			}
		}else{
			$this->form_validation->set_rules('password', 'Nueva Contraseña', 'required');
			$this->form_validation->set_rules('confirmar_password', 'Confirmar contraseña', 'required|matches[password]');
			$data['vista'] = 1;
		}

		$this->form_validation->set_rules('nombre', 'Nombre', 'required');
		$this->form_validation->set_rules('rol', 'rol', 'required');
		$this->form_validation->set_rules('email', 'Correo', 'required|valid_email');

		$this->form_validation->set_message('required', '%s es requerido');
		$this->form_validation->set_message('valid_email', '%s no es un mail valido');
		$this->form_validation->set_message('matches', '%s  no coincide');

		if ($this->form_validation->run() === FALSE) {
			$roles = $this->Usuarios_model->get_roles($_SESSION['rol_id']);
			$data['roles'] = [];
			foreach ($roles as $value) {
				$data['roles'][$value->id] = $value->name;
			}
			$data['errors'] = validation_errors();
			$this->view('admin/usuarios/form', $data);

		}else{

			$save['username'] = strip_tags($this->input->post('nombre'));
			$save['nombres'] = strip_tags($this->input->post('nombres'));
			$save['apellido_paterno'] = strip_tags($this->input->post('apellido_paterno'));
			$save['apellido_materno'] = strip_tags($this->input->post('apellido_materno'));
			$save['username'] = strip_tags($this->input->post('nombre'));
			$save['role_id'] = (int)$this->input->post('rol');
			$save['email'] = strip_tags($this->input->post('email'));
			$save['active'] = (int)$this->input->post('estatus');
			$pass = $this->input->post('password');
			// $pass_confirm = $this->input->post('confirmar_password');

			if ($usuario_id) {
				if ($pass || $password) {
					$save['password_hash'] = $this->auth->hash_password($pass);
					$correo =  $this->enviar_correo($save['username'], $save['email'], $pass, 'modificado');
					if ($correo  == false) {
						$this->session->set_flashdata('errors', 'No se puedo enviar el correo');
					}
				}
				$save['modified_date'] = date("Y-m-d") ;
				$data_update = $this->Usuarios_model->update_usuario($save, $usuario_id);
				if ($data_update) {
					$this->session->set_flashdata('messages', 'Los datos han sido actualizados');
				} else {
					$this->session->set_flashdata('errors', 'Los datos no han podido ser guardados');
				}
				redirect('/admin/usuarios');
			}
			else{
				$save['password_hash'] = $this->auth->hash_password($pass);
				$save['created_date'] = date("Y-m-d") ;
				$data_save = $this->Usuarios_model->insert_usuario($save);
				if ($data_save) {
					$this->session->set_flashdata('messages', 'Los datos han sido guardados');
					$correo =  $this->enviar_correo($save['username'], $save['email'], $pass, 'nuevo');
				} else {
					$this->session->set_flashdata('errors', 'Los datos no han podido ser guardados');
				}
				redirect('/admin/usuarios');
			}

		}

	}

	public function enviar_correo($nombre, $email, $pass, $type){
		$mensaje = $type == 'nuevo' ? '<br><b>Asunto: </b>' . ' Bienvenido al sistema de Kba'. '<br><b>Mensaje:</b>Tu Usuario es <b>'.$nombre.'</b> tu contraseña es la siguiente <b>' .$pass. '</b>' : '<b>Usuario: </b> ' . $nombre. '<br><b>Asunto: </b>' . 'cambio de contraseña'. '<br><b>Mensaje: </b> tu contraseña a sido actulizada por la siguiente <b>' .$pass. '</b>' ;
		$msg .= $mensaje;

		$this->load->library('email');

		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';

		$this->email->initialize($config);
		$this->email->from('test@test.com', 'KBA - Contacto');
		$this->email->to(" $email , fu-sio@hotmail.com");
		$this->email->reply_to('test@test.com');
		$this->email->subject('Contacto KBA');
		$this->email->message($msg);
		$s = $this->email->send();

		if ($s) {
			return true;
		}else{
			return false;
		}
	}

	public function eliminar_usuario($usuario_id = FALSE)
	{
		$usuario_id = strip_tags($this->input->post('usuario'));
		$usuario_id = substr($usuario_id, 4);
		$eliminado = $this->Usuarios_model->delete_usuario($usuario_id);
		echo (json_encode($eliminado));
	}

	public function info_usuario($usuario_id = FALSE)
	{
		$info_usuario = $this->Usuarios_model->info_usuario_id($usuario_id);
		$this->output->set_content_type('application/json');
		echo json_encode($info_usuario[0]);
	}

}
