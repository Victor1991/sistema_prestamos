<?php
/**
* @VictorHugo
* 02/01/2020
*/
class Proyectos extends MY_Controller{

     function __construct(){
          parent::__construct();
          $session = $this->auth->is_logged_in();
          if ($session == FALSE) {
               redirect('login');
          }

          $this->seccion = '6';
          $this->nombre_seccion = 'Proyectos';
          $this->load->model(['proyectos_model']);
     }

     public function index(){
          $this->load->helper('form');
          $data['username'] = "";
          $data['errors'] = $this->session->flashdata('errors');
          $data['messages'] = $this->session->flashdata('messages');

          $this->add_asset('js', 'js/plugins/jquery-bootgrid/jquery.bootgrid.js');
          $this->add_asset('js', 'js/plugins/jquery-bootgrid/jquery.bootgrid.fa.js');
          $this->add_asset('css', 'js/plugins/jquery-bootgrid/jquery.bootgrid.css');

          $this->add_asset('js', 'js/plugins/posts/admin-posts.js');
          $this->add_asset('js', 'js/plugins/sweetalert/sweetalert.min.js');
          $this->add_asset('css', 'css/plugins/sweetalert/sweetalert.css');
          $this->add_asset('js', 'js/lista_proyectos.js');

          $this->view('admin/proyectos/lista', $data);
     }

     public function form($proyecto_id = FALSE) {
          $this->load->library('form_validation');
          $this->load->helper('form');
          //variable para textos
          $data['titulo_form'] = 'Nuevo proyecto';
          $data['errors'] = $this->session->flashdata('errors');
          $data['messages'] = $this->session->flashdata('messages');
          //variable para datos
          $data['id'] = $proyecto_id;
          $data['nombre'] = "";
          $data['interes'] = "";
          $data['estatus'] = "";
          $data['descripcion'] = "";
          if ($proyecto_id) {
               $data['titulo_form'] = 'Editar proyecto';
               $data  = array_merge($data, (array)$this->proyectos_model->get_by_id($proyecto_id));
          }
          $this->form_validation->set_rules('nombre', 'Nombre', 'required');
          $this->form_validation->set_rules('interes', 'Interes', 'required');
          $this->form_validation->set_message('required', '%s es requerido');
          if ($this->form_validation->run() === FALSE) {
               $data['errors'] = validation_errors();
               $this->view('admin/proyectos/form', $data);
          }else{
               $save['nombre'] = strip_tags($this->input->post('nombre'));
               $save['interes'] = strip_tags($this->input->post('interes'));
               $save['estatus'] = strip_tags($this->input->post('estatus'));
               $save['descripcion'] = strip_tags($this->input->post('descripcion'));

               if ($proyecto_id) {
                    $save['fecha_edicion'] = date("Y-m-d");
                    $data_update = $this->proyectos_model->update($proyecto_id, $save);
                    if ($data_update) {
                         $this->session->set_flashdata('messages', 'Los datos han sido actualizados');
                    }else{
                         $this->session->set_flashdata('errors', 'Los datos no han podido ser guardados');
                    }
                    redirect('/admin/proyectos');
               }
               else{
                    $save['fecha_creacion'] = date("Y-m-d");
                    $data_save = $this->proyectos_model->insert($save);
                    if ($data_save) {
                         $this->session->set_flashdata('messages', 'Los datos han sido guardados');
                    } else {
                         $this->session->set_flashdata('errors', 'Los datos no han podido ser guardados');
                    }
                    redirect('/admin/proyectos');
               }
          }
     }


     public function tabla_principal(){
          $this->load->library('pagination');
          $pro_page = 10;
          $offset = (int)$this->input->get('per_page');
          $offset_enviar = $pro_page * $offset;
          $where = array('active' => 0);
          $orden = array('columna' => 'nombre', 'orden' => 'desc');
          if ($this->input->get('orden')) {
               $orden['columna'] = $this->input->get('columna');
               $orden['orden'] = $this->input->get('orden');
          }
          // Buscador por nombre
          if ($this->input->get('username')) {
               $where['username'] = $this->input->get('username');
               $data['username'] = $this->input->get('username');
          }
          //$config['base_url'] = site_url('admin/usuarios/');
          //$config['reuse_query_string'] = true;
          $config['page_query_string'] = TRUE;
          $config['base_url'] = 'table_usuarios';
          $config['total_rows'] = $this->proyectos_model->count_all($where);
          $config['per_page'] = $pro_page;
          $config['num_links'] = 5;
          $config['uri_segment'] = 3;
          $config['first_link'] = 'Primero';
          $config['first_tag_open'] = '<li>';
          $config['first_tag_close'] = '</li>';
          $config['last_link'] = 'Último';
          $config['last_tag_open'] = '<li>';
          $config['last_tag_close'] = '</li>';
          $config['full_tag_open'] = '<nav><ul class="pagination nobottommargin">';
          $config['full_tag_close'] = '</ul></nav>';
          $config['cur_tag_open'] = '<li class="active"><a href="#">';
          $config['cur_tag_close'] = '</a></li>';
          $config['num_tag_open'] = '<li>';
          $config['num_tag_close'] = '</li>';
          $config['prev_link'] = '&laquo;';
          $config['prev_tag_open'] = '<li>';
          $config['prev_tag_close'] = '</li>';
          $config['next_link'] = '&raquo;';
          $config['next_tag_open'] = '<li>';
          $config['next_tag_close'] = '</li>';
          $this->pagination->initialize($config);
          $page_link = $this->pagination->create_links();
          $list = $this->proyectos_model->get_all($where, $orden, $pro_page, $offset);
          // _dump($list);
          $table = '';

          foreach($list as $row):
               $span = $row->estatus == 1 ? '<h5><span class="badge badge-success">Activo</span></h5>' : '<h5><span class="badge badge-danger">Inactivo</span></h5>';
               $table .= '<tr>';
               $table .= '<td>'.$row->id.'</td>';
               $table .= '<td>'.$row->nombre.'</td>';
               $table .= '<td>'. number_format($row->interes, 2, '.', ',').' %</td>';
               $table .= '<td>'.$span.'</td>';

               $table .= '<td class="center">
                              <a href="proyectos/form/'.$row->id.'" class="btn btn-info btn-sm " title="Editar"><i class="fa fa-edit"></i></a>
                              <a href="proyectos/form/'.$row->id.'" class="btn btn-primary btn-sm " title="Reporte"><i class="fas fa-chart-line"></i></a>
                         </td>';
               $table  .= '</tr>';
          endforeach;
          $data = array('table' => $table, 'page' => $page_link);
          $this->output->set_content_type('application/json');
          echo json_encode($data);
     }


}

?>
