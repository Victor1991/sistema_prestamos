<?php
class Inversionistas extends MY_Controller{
     public function __construct(){
          parent::__construct();
          $session = $this->auth->is_logged_in();
          if ($session == FALSE) {
               redirect('login');
          }

          $this->seccion = '7';
          $this->nombre_seccion = 'Inversionistas';
          $this->load->model(array('proyectos_model', 'clientes_model', 'catalogos_model', 'aportaciones_model'));
     }

     public function index(){
          $this->load->helper('form');
          $data['username'] = "";
          $data['errors'] = $this->session->flashdata('errors');
          $data['messages'] = $this->session->flashdata('messages');

          $this->add_asset('js', 'js/plugins/jquery-bootgrid/jquery.bootgrid.js');
          $this->add_asset('js', 'js/plugins/jquery-bootgrid/jquery.bootgrid.fa.js');
          $this->add_asset('css', 'js/plugins/jquery-bootgrid/jquery.bootgrid.css');

          $this->add_asset('js', 'js/plugins/posts/admin-posts.js');
          $this->add_asset('js', 'js/plugins/sweetalert/sweetalert.min.js');
          $this->add_asset('css', 'css/plugins/sweetalert/sweetalert.css');
          $this->add_asset('js', 'js/lista_inversionistas.js');

          $this->view('admin/inversionistas/lista', $data);
     }

     public function inversiones_cliente($cliente_id){
          $data['proyectos'] = $this->proyectos_model->get_proyects();
          $data['cliente'] = $this->clientes_model->get_by_id($cliente_id);
          $data['formas_pago'] = $this->catalogos_model->get_tipo_pago();
          $data['formas_aportaciones'] = $this->catalogos_model->get_tipo_aportacion();
          $this->add_asset('js', 'js/inversiones_cliente.js');
          $this->view('admin/inversionistas/inversiones_cliente', $data);
     }

     public function tabla_principal(){
          $this->load->library('pagination');
          $pro_page = 10;
          $offset = (int)$this->input->get('per_page');
          $offset_enviar = $pro_page * $offset;
          $where = array('active' => 0);
          $where['inversinista'] = 1 ;
          $orden = array('columna' => 'nombres', 'orden' => 'desc');
          if ($this->input->get('orden')) {
               $orden['columna'] = $this->input->get('columna');
               $orden['orden'] = $this->input->get('orden');
          }
          // Buscador por nombre
          if ($this->input->get('username')) {
               $where['username'] = $this->input->get('username');
               $data['username'] = $this->input->get('username');
          }

          $config['page_query_string'] = TRUE;
          $config['base_url'] = 'table_usuarios';
          $config['total_rows'] = $this->clientes_model->num_clientes($where);
          $config['per_page'] = $pro_page;
          $config['num_links'] = 5;
          $config['uri_segment'] = 3;
          $config['first_link'] = 'Primero';
          $config['first_tag_open'] = '<li>';
          $config['first_tag_close'] = '</li>';
          $config['last_link'] = 'Último';
          $config['last_tag_open'] = '<li>';
          $config['last_tag_close'] = '</li>';
          $config['full_tag_open'] = '<nav><ul class="pagination nobottommargin">';
          $config['full_tag_close'] = '</ul></nav>';
          $config['cur_tag_open'] = '<li class="active"><a href="#">';
          $config['cur_tag_close'] = '</a></li>';
          $config['num_tag_open'] = '<li>';
          $config['num_tag_close'] = '</li>';
          $config['prev_link'] = '&laquo;';
          $config['prev_tag_open'] = '<li>';
          $config['prev_tag_close'] = '</li>';
          $config['next_link'] = '&raquo;';
          $config['next_tag_open'] = '<li>';
          $config['next_tag_close'] = '</li>';

          $this->pagination->initialize($config);
          $page_link = $this->pagination->create_links();
          $list = $this->clientes_model->get_many($where, $orden, $pro_page, $offset);
          // _dump($this->db->last_query());
          // _dump($list);
          $table = '';

          foreach($list as $row):
               $table.='<tr>';
               $table.='<td>'.$row->nombres.' '.$row->apellido_paterno.''.$row->apellido_materno.'</td>';
               $table.='<td>'.$row->telefono	.'</td>';
               $table.='<td>'.$row->celular.'</td>';

               // ver botones
               $table.='<td class="center"><a href="inversionistas/inversiones_cliente/'.$row->id.'" class="btn btn-info btn-sm " title="Ver aportaciones"><i class="fas fa-balance-scale"></i></a></td>';

               $table.='</tr>';

          endforeach;


          $data = array('table' => $table, 'page' => $page_link);
          $this->output->set_content_type('application/json');
          echo json_encode($data);
     }


     public function guardar_aportacion(){

          $this->load->library('form_validation');
          $this->form_validation->set_rules('proyecto_id','Proyecto','trim|required');
          $this->form_validation->set_rules('tipo_pago','Tipo de pago','trim|required');
          $this->form_validation->set_rules('cantidad','Cantidad','trim|required');
          $this->form_validation->set_rules('tipo_aportacion','Tipo de aportación','trim|required');
          $this->form_validation->set_rules('fecha','Fecha','trim|required');
          $this->form_validation->set_rules('interes','Interes','trim|required');

          $respuesta = array();
          if ($this->form_validation->run() == TRUE) {
               $save['proyecto_id'] = $this->input->post('proyecto_id');
               $save['usuario_id'] = $this->input->post('cliente_id');
               $save['cat_pago_id'] = $this->input->post('tipo_pago');
               $save['cantidad'] = $this->input->post('cantidad');
               $save['cat_aport_id'] = $this->input->post('tipo_aportacion');
               $save['interes'] = $this->input->post('interes');
               $save['fecha'] = $this->input->post('fecha');
               if ($this->input->post('id')) {

                    $saved = $this->aportaciones_model->update($this->input->post('id'), $save);
               }else{
                    $saved = $this->aportaciones_model->insert($save);
               }
               if ($saved) {
                    $respuesta = array(
                         'tipo' => 'exito',
                         'mensaje' => 'Éxito al guardar la información'
                    );
               }else{
                    $respuesta = array(
                         'tipo' => 'error',
                         'mensaje' => 'Error al guardar la información'
                    );
               }

          }else{
               $respuesta = array(
                    'tipo' => 'validacion',
                    'mensaje' => $this->form_validation->error_array()
               );
          }
          echo json_encode($respuesta, true);
     }


     public function get_aportaciones_cliente(){
          $cliente_id = $this->input->post('cliente_id');
          $aportaciones = array();
          $aportaciones = $this->aportaciones_model->get_aportaciones_cliente($cliente_id);
          echo json_encode($aportaciones);
     }


}
