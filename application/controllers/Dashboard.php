<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $session = $this->auth->is_logged_in();
		if ($session == FALSE) {
             redirect('login');
        }
        $this->load->model(['clientes_model', 'prestamos_model']);
        $this->nombre_seccion = 'Dashboard';
    }

    public function index() {
        $data['errors'] = $this->session->flashdata('errors');
        $data['messages'] = $this->session->flashdata('messages');
        $data['clientes'] = $this->clientes_model->num_clientes();
        $data['prestamos'] = $this->prestamos_model->num_clientes();
        $this->view('admin/dashboard/index', $data);
    }

    public function indx_anterior() {
        $this->view('dashboard');
    }

}
