<?php

class Login extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->lang->load('usuarios');
        $this->load->library(array('auth', 'form_validation'));
        $this->load->helper(array('form', 'alert'));
    }

    public function index() {
        $data = array();
        $this->form_validation->set_rules('username', $this->lang->line('username'), 'trim|required');
        $this->form_validation->set_rules('password', $this->lang->line('password'), 'trim|required');

        if ($this->form_validation->run()) {
            if ($this->auth->login($this->input->post('username'), $this->input->post('password'))) {
                redirect('/admin/', 'refresh');
            } else {
                $this->session->set_flashdata('errores', $this->auth->errores());
                redirect('login', 'refresh');
            }
        } else {
            $data['errores'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('errores');
            $data['mensajes'] = $this->session->flashdata('mensajes');
        }

        $this->load->view('admin/login/login', $data);
    }

    public function logout() {
        $this->session->set_userdata('logged_in', FALSE);
        $session_vars = array('usuario_username','usuario_email','usuario_id','ultimo_login');
        $this->session->unset_userdata($session_vars);
        redirect(base_url("login"));
    }

    public function recuperar_password() {
        if ($this->input->is_ajax_request()) {
            $response = array(
                'estatus' => 'ok',
                'mensaje' => ''
            );

            $this->form_validation->set_rules('correo', 'Correo', 'required|trim|valid_email');
            if ($this->form_validation->run()) {
                if ($this->auth->recuperar_pass($this->input->post('correo'))) {
                    $response['mensaje'] = 'Se le ha enviado un correo con las intrucciones para recuperar su contraseña.';
                } else {
                    $response['estatus'] = 'error';
                    $response['mensaje'] = $this->auth->_errores;
                }
            } else {
                $response['estatus'] = 'error';
                $response['mensaje'] = validation_errors();
            }

            header('Content-type: text/json');
            echo json_encode($response);
        } else {
            $this->load->view('admin/login/recuperar_pass');
        }
    }

    public function actualizar_password($username, $codigo) {
        $username = htmlentities($username, ENT_QUOTES);
        $codigo = htmlentities($codigo, ENT_QUOTES);

        if (!$this->auth->validar_codigo_cambiar_pass($username, $codigo)) {
            $this->session->set_flashdata('errores', 'El código usado para recuperar su contraseña no es válido.');
            redirect('login');
        }

        $this->form_validation->set_rules('password', 'Contraseña', 'trim|required|min_length[8]');
        $this->form_validation->set_rules('conf_password', 'Confirmar contraseña', 'matches[password]');

        if ($this->form_validation->run()) {
            if ($this->auth->cambiar_password($username, $this->input->post('password'))) {
                $this->session->set_flashdata('mensajes', 'Su contraseña ha sido modificada, ahora puede iniciar sesión con su nueva contraseña.');
            } else {
                $this->session->set_flashdata('errores', 'No se pudo cambiar su contraseña, vuelva a intentarlo por favor. '
                        . 'Si persiste el problema contacte al administrador.');
            }
            $data['mensaje_cambio'] = TRUE;
            $data['url_accion'] = '';
            $this->load->view('admin/login/actualizar_pass', $data);
        } else {
            $data['mensaje_cambio'] = FALSE;
            $data['url_accion'] = base_url('login/actualizar_password/'.$username.'/'.$codigo);
            $this->session->set_flashdata('errores', validation_errors());
            $this->load->view('admin/login/actualizar_pass', $data);
        }
    }

    public function nuevo_password() {
        if ($this->form_validation->run()) {
            if ($this->auth->cambiar_password()) {
                $this->session->set_flashdata('mensajes', 'Su contraseña ha sido modificada.');
            } else {
                $this->session->set_flashdata('errores', 'No se pudo cambiar su contraseña.');
            }
        } else {
        }
    }

    public function info() {
        phpinfo();
    }

}
