$(document).ready(function() {
    var maxLen = $('#resumen').data('max-len');
    $('#resumen').maxlength({
        feedbackText: '{r} caracteres restantes ({m} máximo)',
        max: maxLen
    });
    $('#tags').tagsInput({
        autocomplete_url:'/admin/posts/tag_autocomplete',
        autocomplete:{minLength:3,position: { collision: 'flip' }},
        width:'100%',
        height:'150px',
        defaultText:'Añadir etiqueta'
    });
    
    $("#btn-guardar").click(function (e) {
        show_load_submit();
    });
    $('.select2-simple').select2({width:'100%'});
    
    
    
//  Comprobar tamaño imagen antes de subir archivo    
//    $("#btn-guardar").click(function (e) {
//        e.preventDefault();
//        //Get reference of FileUpload.
//        var fileUpload = $("#imagen")[0];
//
//        //Check whether the file is valid Image.
//        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
//        if (fileUpload.value != '' && regex.test(fileUpload.value.toLowerCase())) {
//            //Check whether HTML5 is supported.
//            if (typeof (fileUpload.files) != "undefined") {
//                //Initiate the FileReader object.
//                var reader = new FileReader();
//                //Read the contents of Image File.
//                reader.readAsDataURL(fileUpload.files[0]);
//                reader.onload = function (e) {
//                    //Initiate the JavaScript Image object.
//                    var image = new Image();
//                    //Set the Base64 string return from FileReader as source.
//                    image.src = e.target.result;
//                    image.onload = function () {
//                        //Determine the Height and Width.
//                        var height = this.height;
//                        var width = this.width;
//                        if (height != $("#imagen").data('height') || width != $("#imagen").data('width')) {
//                            alert("La imagen no tiene el tamaño requerido");
//                            return false;
//                        }
//                        
//                        show_load_submit();
//                        $('#form_banner').submit();
//                        return true;
//                    };
//                }
//            } else {
//                alert("El navegador no soporta HTML5.");
//                return false;
//            }
//        } else {
//            alert("Por favor, seleccione una imagen válida.");
//            return false;
//        }
//    });

    
    
//    console.log("Prueba");
    
});

function show_load_submit() {
    $('.form-actions').hide();
    $('.form-loading').show();
}



