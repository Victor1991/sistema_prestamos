﻿
function Eliminar(btn) {
    swal({
        title: "Desea eliminar al usuario ?",
        text: "¡No podrás recuperar el usuario!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'No, cancelar',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    },
    function(){   
        $.post("usuarios/eliminar_usuario/",
        {
            usuario: btn.id
        },
        function(data, status){
            console.log(data);
            console.log(status);
            if (status == 'success') {
                swal({title: "Elimiando", text: "El usuario a sido eliminado", type: "success"},
                 function(){ 
                     location.reload();
                 }
                 );
            }
            else{
               swal("Error", "No se puedo elimina el usuario", "error");
           }
       });
    });
}




