$( document ).ready(function() {
     get_hoteles();
});

function get_hoteles() {
     $.ajax({
          url: baseURL+'admin/inversionistas/get_aportaciones_cliente',
          type: 'POST',
          dataType: 'json',
          data: {cliente_id: $('#cliente_id').val()}
     })
     .done(function(respuesta) {
          ver_proyecto(respuesta)
     })
     .fail(function() {
          console.log("error");
     })
     .always(function() {
          console.log("complete");
     });
}

function span_status($status) {
     if ($status == 1) {
          return '<span class="badge badge-success" style=" z-index: 99; position: absolute; font-size: 14px; margin: 10px;">Activo</span>';
     }else{
          return '<span class="badge badge-danger" style=" z-index: 99; position: absolute; font-size: 14px; margin: 10px;">Inactivo</span>';
     }
}


function ver_proyecto(respuesta) {
     // $('#lista_proyectos').html('');

     $.each(respuesta, function( index, value ) {
          $("#lista_proyectos").append('<div class="col-md-6">\n\
                                             <div class="card">\n\
                                                  '+span_status(value.estatus)+'\n\
                                                  <div id="container'+value.id+'" style="width:100%; margin-top:45px; height:400px; background-color: #ddd;"></div>\n\
                                                  <div class="card-body" style="border-top:1px solid #eee;">\n\
                                                       <h5 class="card-title"> <i class="fas fa-business-time"></i> '+value.nom_proyecto+'</h5>\n\
                                                       <p class="card-text"><i class="fas fa-info-circle" ></i> '+value.nom_proyecto+'</p>\n\
                                                  </div>\n\
                                                  <div class="card-footer text-center">\n\
                                                       <div class="row">\n\
                                                            <div class="col-md-6">Aportaciones :<br> <span class="badge badge-success">$'+value.aportacion+'</span></div>\n\
                                                            <div class="col-md-6">Pagos :<br> <span class="badge badge-secondary">$'+value.pago+'</span></div>\n\
                                                       </div>\n\
                                                  </div>\n\
                                             </div>\n\
                                        </div>');
          chart(value.id, value.aportacion, value.pago);
     });
}

function chart($id, $aportaciones, $pagos) {
     let chart1 = Highcharts.chart('container'+$id, {
          chart: {
               type: 'column'
          },
          title: {
               text: ''
          },
          colors: [
               '#1cc88a',
               '#858796',
               '#0000ff'
          ],
          xAxis: {
               type: 'category'
          },
          yAxis: {
               title: {
                    text: 'Pagos generados.'
               }

          },
          legend: {
               enabled: false
          },
          plotOptions: {
               series: {
                    borderWidth: 0,
                    dataLabels: {
                         enabled: true,
                         format: '$ {point.y:.1f}'
                    }
               }
          },

          tooltip: {
               headerFormat: '<span style="font-size:11px">{series.name.2f}</span><br>',
               pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>$ {point.y:.2f}</b><br/>'
          },

          series: [
               {
                    name: "Pagos",
                    colorByPoint: true,
                    data: [
                         {
                              name: "Aprtaciones",
                              y:  parseFloat($aportaciones),
                              drilldown: "Aprtaciones"
                         },
                         {
                              name: "Pagos",
                              y:  parseFloat($pagos),
                              drilldown: "Pagos"
                         }
                    ]
               }
          ]

     });
};


$("#form_aportacion").submit(function(e) {
     var post_url = $(this).attr("action"); //get form action url
     var request_method = $(this).attr("method"); //get form GET/POST method
     var form_data = $(this).serialize(); //Encode form elements for submission
     var data = new FormData(this);
     e.preventDefault(); // avoid to execute the actual submit of the form.
     $.ajax({
          url : post_url,
          type: request_method,
          data : data,
          contentType: false,
          processData: false,
          dataType: "json",
          success: function(data){
               if (data.tipo != 'validacion') {
                    Swal.fire({
                         title: data.tipo.toUpperCase(),
                         text: data.mensaje,
                         type: data.tipo == 'exito' ? 'success' : 'warning'
                    });
                    $('#modal_aportaciones').modal('hide');
                    get_hoteles();
               }else{
                    $.each(data.mensaje, function( index, value ) {
                         $("[name='"+index+"']").after("<span class='valid'>"+value+"</span>");
                    });
               }
          }
     });

});
