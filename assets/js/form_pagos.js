$( document ).ready(function() {
     $('.datepickern').datepicker({
          format: 'yyyy-mm-dd',
          autoHide:true
     });
});


function filterFloat(evt,input){
     // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
     var key = window.Event ? evt.which : evt.keyCode;
     var chark = String.fromCharCode(key);
     var tempValue = input.value+chark;
     if(key >= 48 && key <= 57){
          if(filter(tempValue)=== false){
               return false;
          }else{
               return true;
          }
     }else{
          if(key == 8 || key == 13 || key == 0) {
               return true;
          }else if(key == 46){
               if(filter(tempValue)=== false){
                    return false;
               }else{
                    return true;
               }
          }else{
               return false;
          }
     }
}

function filter(__val__){
     var preg = /^([0-9]+\.?[0-9]{0,2})$/;
     if(preg.test(__val__) === true){
          return true;
     }else{
          return false;
     }

}

$('.solo-numero').keyup(function (){
     this.value = (this.value + '').replace(/[^0-9]/g, '');
});

function ejecutar(){
     var monto = $('#monto').val();
     var enganche = $('#enganche').val();
     var interes = $('#interes').val();
     var meses = $('#meses').val();
     var interes_decimal = 0;
     var tasa_interes = 0;

     if (interes != 0 || interes != '' ) {
          var interes_decimal = interes * 0.01;
          var tasa_interes = interes_decimal / 12;
     }

     var interes_general = (monto - enganche) * tasa_interes;
     $('#interes_general').html(interes_general);
     $('#tasa_interes').html(tasa_interes);
     var n = 1 - Math.pow((1 + tasa_interes), (meses)*(-1));
     $('#n').html(n);
     var cuota_recomendada = parseFloat(interes_general/n).toFixed(2);
     $('#recomendada').html('Cuota recomendada : $ '+cuota_recomendada);
     var cuota = $('#cuota_mensual').val();
     $('#cuota_mensual').html( cuota );


     $('#intereses').val(JSON.stringify({
          tasa_interes: tasa_interes,
          interes_general: interes_general,
          tasa_anual: n,
          cuota_mensual: cuota,
     }));

     crear_tabla(meses, monto, tasa_interes, cuota);
}

$( document ).ready(function() {
     ejecutar();
});


async function crear_tabla(meses, monto, tasa_interes, cuota) {
     $('#myTable').html('');
     var jsonArr = [];
     console.log(monto);
     for (var i = 1; i <= meses; i++) {
          var result = await append_table(i, meses, monto, tasa_interes, cuota);
          monto = result[1];
          tasa_interes = result[2];
          cuota = result[3];
     };

     $('#tabla').val(JSON.stringify(jsonArr));

}

function append_table(i, meses, monto, tasa_interes, cuota) {
     return new Promise(resolve => {
          var current = new Date($('#fecha_inicio').val()+'T12:00:00Z');
          var nueva_fecha = new Date(new Date(current).setMonth(current.getMonth() + i));
          interes = parseFloat(monto * tasa_interes).toFixed(2);
          // amortizacion = parseFloat(cuota - interes).toFixed(2);
          amortizacion = 0;
          $.ajax({
               data: {
                    "prestamo_id" : $('#prestamo_id').val(),
                    "mes" :nueva_fecha.getMonth()+1,
                    "anio" : nueva_fecha.getFullYear()
               },
               type: "POST",
               dataType: "json",
               url: baseURL + "admin/pagos/pagos_prestamos",
          }).done(function( data, textStatus, jqXHR ) {
               var pagos = '';
               var sum_pagos = 0;

               $.each( data.pago, function( key, value ) {
                    sum_pagos += parseFloat(value.monto);
                    var _fecha = new Date(value.fecha_pago+'T12:00:00Z')
                    var foto =  value.imagen === null || value.imagen == ''  ? '' : ' / <i class="fas fa-photo-video" onclick="var_imagen(\''+value.imagen+'\')"></i>' ;
                    pagos += '<span class="badge badge-primary">'+moneda(value.monto)+' / '+value.cat_pago+' / '+formatDate(_fecha, true) + foto +'</span><br>';
               });


               amortizacion = parseFloat(cuota -  interes).toFixed(2);;

               var current = new Date(data.fecha);
               current.setMinutes(current.getMinutes() + current.getTimezoneOffset())

               console.log();

               $('#myTable').append('<tr>\n\
               <td>'+i+'</td>\n\
               <td>'+ moneda(parseFloat(monto).toFixed(2))+'</td>\n\
               <td>'+moneda(interes)+'</td>\n\
               <td>'+moneda(cuota)+'</td>\n\
               <td>'+moneda(cuota - interes )+'</td>\n\
               <td>'+formatDate(current, false)+'</td>\n\
               <td>'+pagos+'</td>\n\
               </tr>');


               pag = interes - sum_pagos;

               if (pag < 0) {
                    monto = monto - (pag*-1);
               }else{
                    monto = parseFloat(monto) + parseFloat(pag);
               }
               resolve([data.fecha, monto, tasa_interes, cuota ]);
          });
     });
}

function var_imagen($img) {
     var modalImg = document.getElementById("img01");
     modalImg.src = baseURL +'/uploads/'+$img;;
     $('#modalimagen').modal('show');

}

function moneda(value) {
     return '$ '+Number(parseFloat(value).toFixed(2)).toLocaleString('en', {minimumFractionDigits: 2})
}

function parse(value) {
     return Number(parseFloat(value).toFixed(2)).toLocaleString('en', {minimumFractionDigits: 2})
}

function formatDate(date, ver_dia ) {
     console.log(date);
     var monthNames = [
          "Enero", "Febrero", "Marzo",
          "Abril", "Mayo", "Junio", "Julio",
          "Agosto", "Septiembre", "Octubre",
          "Noviembre", "Diciembre"
     ];

     var day = date.getDate();
     var monthIndex = date.getMonth();
     var year = date.getFullYear();
     if (ver_dia) {
          return day+'-'+ monthNames[monthIndex] + '-' + year;
     }else{
          return monthNames[monthIndex] + '-' + year;
     }
}


function pagar() {
     console.log('paso');
     $('#modal_pago').modal('show');
}

$("#form_pago").submit(function(event){

     var post_url = $(this).attr("action"); //get form action url
     var request_method = $(this).attr("method"); //get form GET/POST method
     var form_data = $(this).serialize(); //Encode form elements for submission

      var data = new FormData(this);

      event.preventDefault(); //prevent default action

     $.ajax({
          url : post_url,
          type: request_method,
          data : data,
          contentType: false,
          processData: false,
          dataType: "json",
          beforeSend: function () {
               $('#load').show();
               $('.chat-btn-actions').hide();
          },
          success: function(response) {
               console.log(response);
               if (response.type == 'exito') {
                    Swal.fire({
                         title: 'Exito al crear el prestamo',
                         text: response.mensaje,
                         type: 'success',
                         allowOutsideClick: false,
                    }).then(function() {
                         ejecutar();
                         $('#modal_pago').modal('hide');
                    });
               }else{
                    Swal.fire({
                         title: 'Error al crear el prestamo',
                         text: response.mensaje,
                         type:'error',
                         allowOutsideClick: false
                    }).then(function() {
                         window.location = baseURL+ "/admin/prestamos";
                    });
               }
               console.log(response);
          }.bind(this),
          complete: function() {
               $('.chat-send-comment').hide();
               $('#load').hide(1000);

          }
     });

});
