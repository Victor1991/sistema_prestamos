$(document).ready(function () {
       cargar_tabla();
       $("#tbl-posts").bootgrid().on("click.rs.jquery.bootgrid", function (e)
       {
          var get_orden = $("#tbl-posts").bootgrid("getSortDictionary");
          $.each(get_orden, function(key, value) {
               COLUMNA = key;
               ORDEN = value;
          });
          cargar_tabla();
       });
       $('#tbl-posts-header').hide();
       $('#tbl-posts-footer').hide();

   });

   function cargar_tabla(page_num) {
        $("#tcontent").bootgrid('destroy');
       if (page_num) {
          page_num = 'per_page=' + page_num;
       }
       var filtros = "";
       if (ROL) {
          filtros = '&role_id=' + ROL;
       }
       if (BUSCADOR) {
          filtros = filtros + '&username=' + BUSCADOR;
       }
       if (ORDEN) {
          filtros = filtros + '&columna=' + COLUMNA;
          filtros = filtros + '&orden=' + ORDEN;
       }

       $.getJSON('clientes/table_clientes?'+page_num+ filtros, function(result){
          $("#tcontent").html(result.table);
          $("#page").html(result.page);
       });
   }

   var COLUMNA;
   var ORDEN;
   var ROL;
   var BUSCADOR;

   function filtro() {
       BUSCADOR = $("#username").val();
       ROL = $("#form_rol").val();
       cargar_tabla();
   }

   $('body').on('click', 'ul.pagination a', function(event) {
       event.preventDefault();
       var info = $(this);
       href = info[0].href;
       var url = new URL(href);
       pagina = url.searchParams.get("per_page");
       cargar_tabla(pagina);
   });

   function Cancelar() {
       BUSCADOR = "";
       ROL = "";
       $("#username").val('');
       $("#form_rol").val(0);
       cargar_tabla();
   }

    $('body').on('click', 'button.informacion', function(event) {
       event.preventDefault();
       var info = $(this);
       id_boton = info[0].id;
       id_usuario = id_boton.substring(4);
       perfil(id_usuario);
   });

   function perfil(id_usuario) {
       $.getJSON('usuarios/info_usuario/'+id_usuario, function(info_usuario){
          $("#nombre_modal").html(info_usuario.nombre_completo);
          console.log(info_usuario.active);

          $("#rol_modal").html(info_usuario.rol);
          if (info_usuario.active == 1) { estatus = '<span class="label label-success">ON</span>'; } else { estatus = '<span class="label label-danger">OFF</span>'; };
          $("#estatus_modal").html(estatus);
          $("#correo_modal").html(info_usuario.email);
       });
   }

    function borrar_fomm() {
       $("#nombre_modal").html('');
       $("#rol_modal").html('');
       $("#estatus_modal").html('');
       $("#correo_modal").html('');
    }

    function eliminar(id) {
         swal({
              title: "Eliminar cliente",
              text: "¿ Desea eliminar al cliente?",
              type: "warning",
              buttons: true,
              showCancelButton: true,
              cancelButtonText: "Mmm... mejor no",
              confirmButtonColor: "#36b9cc",
              cancelButtonColor: "#e74a3b",
              confirmButtonText: "¡Adelante!",
              dangerMode: true,
         }, function(value) {
             if (value) {
                  window.location.href = baseURL +"admin/clientes/eliminar_cliente/"+id ;
             }
         });
    }
