$( document ).ready(function() {
     $('.datepickern').datepicker({
          format: 'yyyy-mm-dd',
          autoHide:true
     });
});

function renovar() {
     $('#renovar_prestamo').modal('show');
}

$( ".action" ).focusout(function() {
     ejecutar();
});

function filterFloat(evt,input){
     // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
     var key = window.Event ? evt.which : evt.keyCode;
     var chark = String.fromCharCode(key);
     var tempValue = input.value+chark;
     if(key >= 48 && key <= 57){
          if(filter(tempValue)=== false){
               return false;
          }else{
               return true;
          }
     }else{
          if(key == 8 || key == 13 || key == 0) {
               return true;
          }else if(key == 46){
               if(filter(tempValue)=== false){
                    return false;
               }else{
                    return true;
               }
          }else{
               return false;
          }
     }
}


$('.solo-numero').keyup(function (){
     this.value = this.value.replace(/[^0-9,.]/g, '').replace(/,/g, '.');
});

function ejecutar(){
     var monto_aud = $('#modal_monto_adeudo').val();
     var monto_pre = $('#modal_cantidad_prestar').val();
     var monto = 0;
     if (monto_aud) {
           monto += parseFloat(monto_aud);
     }
     if (monto_pre) {
           monto += parseFloat(monto_pre);
     }

     $('#modal_total').val(monto);
     var enganche = $('#modal_nuevo_enganche').val();
     var interes = $('#modal_interes').val();
     var meses = $('#modal_meses').val();
     // var meses = $('#modal_fecha_inicio').val();
     // var meses = $('#modal_cuota_mensual').val();
     var interes_decimal = 0;
     var tasa_interes = 0;

     if (interes != 0 || interes != '' ) {
          var interes_decimal = interes * 0.01;
          var tasa_interes = interes_decimal / 12;
     }

     console.log(monto);
     console.log(enganche);
     console.log(tasa_interes);

     var interes_general = (monto - enganche) * tasa_interes;

     console.log(tasa_interes);
     console.log(meses);

     var n = 1 - Math.pow((1 + tasa_interes), (meses)*(-1));

     console.log(n);

     $('#n').html(n);
     var cuota_recomendada = parseFloat(interes_general/n).toFixed(2);
     $('#recomendada').html('Cuota recomendada : $ '+cuota_recomendada);
     var cuota = $('#cuota_mensual').val();
     $('#modal_cuota_mensual').html( cuota );


     $('#intereses').val(JSON.stringify({
          tasa_interes: tasa_interes,
          interes_general: interes_general,
          tasa_anual: n,
          cuota_mensual: cuota,
     }));

     crear_tabla(meses, monto, tasa_interes, cuota);
}


function crear_tabla(meses, monto, tasa_interes, cuota) {
     $('#myTable').html('');

     var jsonArr = [];

     for (var i = 1; i <= meses; i++) {
          var current = new Date($('#modal_fecha_inicio').val()+'T12:00:00Z');
          console.log($('#modal_fecha_inicio').val());
          console.log(current);
          var date = new Date(new Date(current).setMonth(current.getMonth() + i));
          console.log(date);

          interes = parseFloat(monto*tasa_interes).toFixed(2);
          amortizacion = parseFloat(cuota -  interes).toFixed(2);;
          $('#myTable').append('<tr>\n\
          <td>'+i+'</td>\n\
          <td>'+ moneda(parseFloat(monto).toFixed(2))+'</td>\n\
          <td>'+moneda(interes)+'</td>\n\
          <td>'+moneda(cuota)+'</td>\n\
          <td>'+moneda(amortizacion)+'</td>\n\
          <td>'+formatDate(date)+'</td>\n\
          </tr>');
          monto  =  monto - amortizacion;

          jsonArr.push({
               num_pago: i,
               capital: parse(parseFloat(monto).toFixed(2)),
               interes: parse(interes),
               cuota: parse(cuota),
               amortizacion: parse(amortizacion),
               fecha_pago: date
          });
     }

     $('#tabla').val(JSON.stringify(jsonArr));
}

function filter(__val__){
     var preg = /^([0-9]+\.?[0-9]{0,2})$/;
     if(preg.test(__val__) === true){
          return true;
     }else{
          return false;
     }
}


function moneda(value) {
     return '$ '+Number(parseFloat(value).toFixed(2)).toLocaleString('en', {minimumFractionDigits: 2})
}

function parse(value) {
     return Number(parseFloat(value).toFixed(2)).toLocaleString('en', {minimumFractionDigits: 2})
}

function formatDate(date) {
     console.log(date);
     var monthNames = [
          "Enero", "Febrero", "Marzo",
          "Abril", "Mayo", "Junio", "Julio",
          "Agosto", "Septiembre", "Octubre",
          "Noviembre", "Diciembre"
     ];

     var day = date.getDate();
     var monthIndex = date.getMonth();
     var year = date.getFullYear();

     return  monthNames[monthIndex] + ' ' + year;
}
