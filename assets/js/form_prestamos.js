
$( document ).ready(function() {
     $('.datepickern').datepicker({
          format: 'yyyy-mm-dd',
          autoHide:true
     });
});

$( ".datepickern" ).change(function() {
     ejecutar();
});

function filterFloat(evt,input){
     // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
     var key = window.Event ? evt.which : evt.keyCode;
     var chark = String.fromCharCode(key);
     var tempValue = input.value+chark;
     if(key >= 48 && key <= 57){
          if(filter(tempValue)=== false){
               return false;
          }else{
               return true;
          }
     }else{
          if(key == 8 || key == 13 || key == 0) {
               return true;
          }else if(key == 46){
               if(filter(tempValue)=== false){
                    return false;
               }else{
                    return true;
               }
          }else{
               return false;
          }
     }
}

function filter(__val__){
     var preg = /^([0-9]+\.?[0-9]{0,2})$/;
     if(preg.test(__val__) === true){
          return true;
     }else{
          return false;
     }
}




$('.solo-numero').keyup(function (){
     this.value = this.value.replace(/[^0-9,.]/g, '').replace(/,/g, '.');
});

function ejecutar(){
     var monto = $('#monto').val();
     var enganche = $('#enganche').val();
     var interes = $('#interes').val();
     var meses = $('#meses').val();
     var interes_decimal = 0;
     var tasa_interes = 0;

     if (interes != 0 || interes != '' ) {
          var interes_decimal = interes * 0.01;
          var tasa_interes = interes_decimal / 12;
     }

     var interes_general = (monto - enganche) * tasa_interes;
     $('#interes_general').html(interes_general);
     $('#tasa_interes').html(tasa_interes);
     var n = 1 - Math.pow((1 + tasa_interes), (meses)*(-1));
     $('#n').html(n);
     var cuota_recomendada = parseFloat(interes_general/n).toFixed(2);
     $('#recomendada').html('Cuota recomendada : $ '+cuota_recomendada);
     var cuota = $('#cuota_mensual').val();
     $('#cuota_mensual').html( cuota );


     $('#intereses').val(JSON.stringify({
          tasa_interes: tasa_interes,
          interes_general: interes_general,
          tasa_anual: n,
          cuota_mensual: cuota,
     }));

     crear_tabla(meses, monto, tasa_interes, cuota);
}

$( document ).ready(function() {
     ejecutar();
});

$('.form-control').keyup(function (){
     ejecutar();
});

function crear_tabla(meses, monto, tasa_interes, cuota) {
     $('#myTable').html('');

     var jsonArr = [];

     for (var i = 1; i <= meses; i++) {
          var current = new Date($('#fecha_inicio').val()+'T12:00:00Z');
          var date = new Date(new Date(current).setMonth(current.getMonth() + i));
          interes = parseFloat(monto*tasa_interes).toFixed(2);
          amortizacion = parseFloat(cuota -  interes).toFixed(2);;
          $('#myTable').append('<tr>\n\
          <td>'+i+'</td>\n\
          <td>'+ moneda(parseFloat(monto).toFixed(2))+'</td>\n\
          <td>'+moneda(interes)+'</td>\n\
          <td>'+moneda(cuota)+'</td>\n\
          <td>'+moneda(amortizacion)+'</td>\n\
          <td>'+formatDate(date)+'</td>\n\
          </tr>');
          monto  =  monto - amortizacion;

          jsonArr.push({
               num_pago: i,
               capital: parse(parseFloat(monto).toFixed(2)),
               interes: parse(interes),
               cuota: parse(cuota),
               amortizacion: parse(amortizacion),
               fecha_pago: date
          });
     }

     $('#tabla').val(JSON.stringify(jsonArr));
}

function moneda(value) {
     return '$ '+Number(parseFloat(value).toFixed(2)).toLocaleString('en', {minimumFractionDigits: 2})
}

function parse(value) {
     return Number(parseFloat(value).toFixed(2)).toLocaleString('en', {minimumFractionDigits: 2})
}

function formatDate(date) {
     var monthNames = [
          "Enero", "Febrero", "Marzo",
          "Abril", "Mayo", "Junio", "Julio",
          "Agosto", "Septiembre", "Octubre",
          "Noviembre", "Diciembre"
     ];

     var day = date.getDate();
     var monthIndex = date.getMonth();
     var year = date.getFullYear();

     return  monthNames[monthIndex] + ' ' + year;
}



function validar_cliente() {
     var cliente_id = $('#cliente_id').val();
     if (cliente_id == '' || cliente_id === null) {
          return false;
     }else {
          return true;
     }
}

function seleccionado_cliente() {
     if (validar_cliente() === false) {
          Swal.fire(
               'Requiere un cliente',
               'Se requiere un cliente para realizar el préstamo',
               'warning'
          )
     }else{
          $( "#modal_clientes" ).modal('hide');
          $( "#sub" ).trigger( "click" );
     }
}


$("#form_prestamos").submit(function(event){
     if (validar_cliente()) {
          var post_url = $(this).attr("action"); //get form action url
          var request_method = $(this).attr("method"); //get form GET/POST method
          var form_data = $(this).serialize(); //Encode form elements for submission
          var form_data = form_data.concat('&cliente_id='+$('#cliente_id').val());

          $.ajax({
               url : post_url,
               type: request_method,
               data : form_data,
               dataType: "json",
               beforeSend: function () {
                    $('#load').show(1000);
                    $('.chat-btn-actions').hide();
               },
               success: function(response) {
                    if (response.type == 'exito') {
                         Swal.fire({
                              title: 'Exito al crear el prestamo',
                              text: response.mensaje,
                              type: 'success',
                              allowOutsideClick: false,
                         }).then(function() {
                              window.location = baseURL+ "/admin/prestamos";
                         });
                    }else{
                         Swal.fire({
                              title: 'Error al crear el prestamo',
                              text: response.mensaje,
                              type:'error',
                              allowOutsideClick: false
                         }).then(function() {
                              ejecutar();
                         });
                    }
               }.bind(this),
               complete: function() {
                    $('.chat-send-comment').hide();
                    $('#load').hide(1000);

               }
          });
     }else{
          $( "#modal_clientes" ).modal('show');
     }
     event.preventDefault(); //prevent default action
});
